<?php
/**
 * Created by PhpStorm.
 * User: turganbay
 * Date: 05/07/2019
 * Time: 09:59
 */

namespace App\Services;


use App\Image;

class ImageService
{

    protected $model;

    function store(){
        $image = new Image();
        $image->src = request()->file('images');
        $this->model->images()->save($image);
    }


    public function destroy($id)
    {
        try{
            $image = Image::find($id);
            $image->delete();
            DB::table('imageables')->where('image_id', $id)->delete();
            return true;
        }catch (\Exception $e){

            info($e->getMessage());
            return false;
        }
    }

    /**
     * @param mixed $model
     */
    public function setModel($model): void
    {
        $this->model = $model;
    }
}
