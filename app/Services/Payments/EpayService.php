<?php

namespace App\Services\Payments;


use Exception;

class EpayService
{
    private $invert;
    private $privateKey;
    private $privateKeyPath;
    private $privateKeyPassword;

    private $name;
    private $certId;
    private $merchantId;
    private $orderId;
    private $amount;
    private $email;
    private $lung;
    private $phone;
    private $currency;
    private $postLink;
    private $backLink;
    private $failLink;


    public function loadPrivateKey($filename, $password = NULL)
    {
        if (!is_file($filename)) {
            return false;
        }
        $c = file_get_contents($filename);
        if ($password) {
            $prvKey = openssl_pkey_get_private($c, $password) or die(openssl_error_string());
        } else {
            $prvKey = openssl_get_privatekey($c) or die(openssl_error_string());
        }
        if (is_resource($prvKey)) {
            $this->privateKey = $prvKey;
            return $c;
        }
        return false;
    }

    public function invert()
    {
        $this->invert = 1;
    }

    function reverse($str)
    {
        return strrev($str);
    }

    function sign($str)
    {
        if ($this->privateKey) {
            openssl_sign($str, $out, $this->privateKey);

            if ($this->invert == 1) $out = $this->reverse($out);
            return $out;
        }
    }

    function sign64($str)
    {
        return base64_encode($this->sign($str));
    }

    function check_sign($data, $str, $filename)
    {
        if ($this->invert == 1) $str = $this->reverse($str);
        if (!is_file($filename)) return false;
        $pubKey = file_get_contents($filename);
        return openssl_verify($data, $str, $pubKey);
    }

    function check_sign64($data, $str, $filename)
    {
        return $this->check_sign($data, base64_decode($str), $filename);
    }

    public function getXml()
    {
        $merchant = '<merchant cert_id="'.$this->certId.'" name="'.$this->name.'"><order order_id="'.$this->orderId.'" amount="'.$this->amount.'" currency="'.$this->currency.'"><department merchant_id="'.$this->merchantId.'" amount="'.$this->amount.'" phone="'.$this->phone.'"/></order></merchant>';
        return "<document>{$merchant}<merchant_sign type=\"RSA\">{$this->sign64($merchant)}</merchant_sign></document>";
    }

    public function generateEpayData()
    {
        $this->invert();

        $this->loadPrivateKey($this->privateKeyPath, $this->privateKeyPassword);
        $xml = $this->getXml();

        $data = [
            'url' => config('epay.link') ,
            'signedOrderB64' => base64_encode($xml),
            'emailForEpay' => $this->email,
            'lang' => $this->lung,
            'backLink' => $this->backLink,
            'postLink' => $this->postLink,
            'failureBackLink' => $this->failLink,
            'template' => config('epay.template'),
            'cardName' => config('epay.cart')
        ];

        return $data;
    }

    public function initPayment($order)
    {
        if ($order) {
            $this->orderId = (int)$order->index;
            $this->amount = $order->amount;
            $this->email = $order->user->email;
            $this->phone = $order->phone;
            $this->merchantId = config('epay.merchant_id');
            $this->name = config('epay.shop_name');
            $this->certId = config('epay.cert_id');
            $this->privateKeyPath = storage_path('epay_cert.prv');
            $this->privateKeyPassword = config('epay.private_key_pass');
            $this->lung = app()->getLocale().'';
            $this->currency = config('epay.currency');
            $this->postLink = route('orders.epay.result', ['orderIndex' => $order->index]).'';
            $this->backLink = route('checkout').'?epay=success&orderIndex='.$order->index;
            $this->failLink = route('checkout').'?epay=fail&orderIndex='.$order->index;
        }
    }
}
