<?php

namespace App\Services\Payments;

use GuzzleHttp\Client;

class Elsom
{
    private $server;
    private $culture_info = "ru-Ru";
    private $msisdn;
    private $partner_code;
    private $password;

    public function __construct()
    {
        $this->server = config('elsom.server');
        $this->msisdn = config('elsom.msisdn');
        $this->partner_code = config('elsom.partner_code');
        $this->password = config('elsom.password');
    }

    public function generateOTP($partner_trn_id, $amount, $udf = '', $cheque_no = '', $cashier_no = '')
    {
        $content = [
            'PartnerGenerateOTP' => [
                'CultureInfo' => $this->culture_info,
                'MSISDN' => $this->msisdn,
                'PartnerCode' => $this->partner_code,
                'PartnerTrnID' => $partner_trn_id,
                'Amount' => $amount,
                'Password' => $this->password,
                'UDF' => $udf,
                'ChequeNo' => $cheque_no,
                'CashierNo' => $cashier_no
            ]
        ];


        $client = new Client([
            'base_uri' => $this->server,
            'verify' => false
        ]);

        $res = $client->request('POST', null,
            [
                'headers'=>['Content-Type' => 'text/json; charset=UTF8'],
                'body'=>json_encode($content)
            ]
        )->getBody()->getContents();

        $res = json_decode($res, true);

        $otp = $res['Response']['Result']['OTP'];

        return $otp;
    }

    public function cancelPayment($partner_trn_id)
    {
        $content = [
            'PartnerCancelPayment' => [
                'CultureInfo' => $this->culture_info,
                'MSISDN' => $this->msisdn,
                'PartnerTrnID' => $partner_trn_id,
                'Password' => $this->password
            ]
        ];

        $client = new Client([
            'base_uri' => $this->server,
            'verify' => false
        ]);

        $res = $client->request('POST', null,
            [
                'headers'=>['Content-Type' => 'text/json; charset=UTF8'],
                'body'=>json_encode($content)
            ]
        )->getBody()->getContents();

        $res = json_decode($res, true);

        return $res;
    }

    public function getPaymentStatus($partner_trn_id)
    {
        $content = [
            'PartnerGetPaymentStatus' => [
                'CultureInfo' => $this->culture_info,
                'MSISDN' => $this->msisdn,
                'PartnerTrnID' => $partner_trn_id,
                'Password' => $this->password
            ]
        ];

        $client = new Client([
            'base_uri' => $this->server,
            'verify' => false
        ]);

        $res = $client->request('POST', null,
            [
                'headers'=>['Content-Type' => 'text/json; charset=UTF8'],
                'body'=>json_encode($content)
            ]
        )->getBody()->getContents();

        $res = json_decode($res, true);

        return $res;
    }

}
