<?php

namespace App\Services;

use App\Category;
use App\Filter;
use App\FilterCategory;
use App\Product;
use App\ProductVariation;
use App\User;
use function foo\func;
use GuzzleHttp\Client;

class RetailcrmService
{
    private $baseUrl = 'https://lavanda2.retailcrm.ru/api/v5/';
    private $client;
    private $headers = [
        'Content-type' => 'application/x-www-form-urlencoded'
    ];

    private $params = [
        'apiKey' => 'CtOtMuNaja2HEJjuRJsGSztBRcvNEpXP'
    ];

    public function __construct()
    {
        $this->client = new Client();
    }

    public function postResponse($url)
    {
        return $this->client->post($this->baseUrl.$url, [
                'headers' => $this->headers,
                'body' => http_build_query($this->params)
            ]
        )->getBody()->getContents();
    }

    public function getResponse($uri)
    {
        $response = $this->client
            ->get($this->baseUrl . $uri . $this->getParams())
            ->getBody()
            ->getContents();

        return json_decode($response, true);
    }

    public function getParams(): string
    {
        $params = '';
        foreach ($this->params as $key => $value) {
            if ($key == 'apiKey') {
                $params .= '?' . $key . '=' . $value;
            } else {
                $params .= '&' . $key . '=' . $value;
            }
        }

        return $params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        foreach ($params as $key => $value) {
            $this->params[$key] = $value;
        }
    }
}
