<?php

namespace App\Services;

use App\Admin\Variation;
use App\Category;
use App\Filter;
use App\FilterCategory;
use App\Product;
use App\ProductVariation;
use App\User;
use GuzzleHttp\Client;

class MoyskladService
{
    private $baseUrl = 'https://online.moysklad.ru/api/remap/1.1';
    private $client;
    private $headers;
    private $categoryIds;
    private $userIds;
    private $productIds;
    private $productCategoryIds;

    public function getStarted()
    {
        $this->client = new Client();
        $this->headers = [
            'Authorization' => 'Basic ' . base64_encode('admin@ceo84:admin123'),
            'Content-Type' => 'application/json;charset=utf-8'
        ];
    }

    protected function getBelongsToId($url, $ids)
    {
        $urlExploded = explode('/', $url);
        $idFromUrl = array_pop($urlExploded);
        return isset($ids[$idFromUrl]) ? $ids[$idFromUrl] : null;
    }

    public function getOurDataForSync()
    {
        $this->categoryIds = Category::pluck('id', 'moysklad_id')->toArray();
        $this->userIds = User::pluck('id', 'moysklad_id')->toArray();
        $this->productIds = Product::pluck('id', 'moysklad_id')->toArray();
        $this->productCategoryIds = Product::pluck('category_id', 'id')->toArray();
    }


    //requests

    private function getResponse($url)
    {
        $response = $this->client
            ->get($url, ['headers' => $this->headers])
            ->getBody()
            ->getContents();

        return json_decode($response, true);
    }

    private function postRequest($url, $data)
    {
        return $this->client->post($url, [
                'headers' => $this->headers,
                'body' => \GuzzleHttp\json_encode($data, true)
            ]
        )->getStatusCode();
    }

    private function putRequest($url, $data)
    {
        //TODO is not ready
        return $this->client->put($url, [
                'headers' => $this->headers,
                'body' => \GuzzleHttp\json_encode($data, true)
            ]
        )->getStatusCode();
    }

    private function deleteRequest($url)
    {
        return $this->client->delete($url, [
                'headers' => $this->headers
            ]
        )->getStatusCode();
    }


    //sync

    public function syncUsers()
    {
        $size = 0;
        $offset = 0;
        $limit = 100;

        do {
            $users = $this->getResponse($this->baseUrl . '/entity/employee?offset=' . $offset . '&limit=' . $limit);

            foreach ($users['rows'] as $userRow) {
                $user = User::where('moysklad_id', $userRow['id'])->first();

                $data = [
                    'name' => $userRow['name'],
                    'email' => $userRow['email'],
                    'phone' => $userRow['phone'],
                    'moysklad_id' => $userRow['id'],
                ];

                if ($user) {
                    $user->update($data);
                } else {
                    $data['password'] = bcrypt(rand(100000, 999999));
                    $newUser = User::create($data);
                    $this->userIds[$userRow['id']] = $newUser->id;

                }
            }

            if (!$offset) {
                $size = $users['meta']['size'];
            }

            $size -= $limit;
            $offset += $limit;

        } while ($size > 0);

    }

    public function syncCategories()
    {
        $size = 0;
        $offset = 0;
        $limit = 100;

        do {
            $productFolders = $this->getResponse($this->baseUrl . '/entity/productfolder?offset=' . $offset . '&limit=' . $limit . '&filter=archived=false&order=pathName');
            foreach ($productFolders['rows'] as $productFolder) {
                $this->createCategory($productFolder);
            }

            if (!$offset) {
                $size = $productFolders['meta']['size'];
            }

            $size -= $limit;
            $offset += $limit;
        } while ($size > 0);
    }

    public function syncProducts()
    {
        $size = 0;
        $offset = 0;
        $limit = 100;

        do {
            $products = $this->getResponse($this->baseUrl . '/entity/product?offset=' . $offset . '&limit=' . $limit . '&filter=archived=false');

            foreach ($products['rows'] as $productRow) {
                $this->createProduct($productRow);
            }

            if (!$offset) {
                $size = $products['meta']['size'];
            }

            $size -= $limit;
            $offset += $limit;

        } while ($size > 0);

    }

    public function syncProductVariants()
    {
        $size = 0;
        $offset = 0;
        $limit = 100;

        do {
            $productVariants = $this->getResponse($this->baseUrl . '/entity/variant?offset=' . $offset . '&limit=' . $limit . '&filter=archived=false');
            foreach ($productVariants['rows'] as $productVariantRow) {
                $this->createVariation($productVariantRow);
            }

            if (!$offset) {
                $size = $productVariants['meta']['size'];
            }

            $size -= $limit;
            $offset += $limit;

        } while ($size > 0);
    }


    //create

    private function createCategory($response)
    {
        $category = Category::where('moysklad_id', $response['id'])->first();

        $parentId = null;
        if (isset($response['productFolder'])) {
            $parentId = $this->getBelongsToId($response['productFolder']['meta']['href'], $this->categoryIds);
        }

        if ($category) {
            $dataForUpdate = [
                'name' => $response['name'],
                'parent_id' => $parentId
            ];
            $this->updateCategory($category, $dataForUpdate);
        } else {
            $category = Category::create([
                'moysklad_id' => $response['id'],
                'name' => $response['name'],
                'parent_id' => $parentId,
            ]);
            $this->categoryIds[$response['id']] = $category->id;
        }
    }

    private function createProduct($response)
    {
        $categoryId = null;
        if (isset($response['productFolder'])) {
            $categoryId = $this->getBelongsToId($response['productFolder']['meta']['href'], $this->categoryIds);
        } else {
            info('There is not product folder', $response);
            return false;
        }
        $userId = null;
        if (isset($response['owner'])) {
            $userId = $this->getBelongsToId($response['owner']['meta']['href'], $this->userIds);
        }

        $product = Product::where('moysklad_id', $response['id'])->first();

        if ($product) {
            $dataForUpdate = [
                'name' => $response['name'],
                'category_id' => $categoryId,
                'user_id' => $userId,
                'external_code' => $response['externalCode'],
            ];

            $this->updateProduct($product, $dataForUpdate);
        } else {
            $product = Product::create([
                'moysklad_id' => $response['id'],
                'name' => $response['name'],
                'category_id' => $categoryId,
                'user_id' => $userId,
            ]);
            $this->productIds[$response['id']] = $product->id;
            $this->productCategoryIds[$product->id] = $categoryId;
        }

        if (!$response['modificationsCount']) {
            $productVariation = ProductVariation::where([
                'moysklad_id' => null,
                'product_id' => $product->id
            ])->first();

            $salePrice = (!!$response['salePrices'][0]['value']) ? substr($response['salePrices'][0]['value'], 0, -2) : 0;

            if ($productVariation) {
                $dataForUpdate = [
                    'name' => $response['name'],
                    'sale_price' => $salePrice,
                    'product_id' => $product->id,
                    'external_code' => null,
                ];
                $this->updateVariation($productVariation, $dataForUpdate);
            } else {
                ProductVariation::create([
                    'moysklad_id' => null,
                    'name' => $response['name'],
                    'sale_price' => $salePrice,
                    'product_id' => $product->id
                ]);
            }
        }
    }

    private function createVariation($response)
    {
        $productId = $this->getBelongsToId($response['product']['meta']['href'], $this->productIds);

        if (!$productId){
            info('There is not product', $response);
            return false;
        }

        $productVariation = ProductVariation::where('moysklad_id', $response['id'])->first();
        $salePrice = (!!$response['salePrices'][0]['value']) ? substr($response['salePrices'][0]['value'], 0, -2) : 0;

        if ($productVariation) {
            $dataForUpdate = [
                'name' => $response['name'],
                'sale_price' => $salePrice,
                'product_id' => $productId,
                'external_code' => $response['externalCode'],
            ];
            $this->updateVariation($productVariation, $dataForUpdate);
        } else {
            ProductVariation::where([
                'moysklad_id' => null,
                'product_id' => $productId
            ])->delete();


            $productVariation = ProductVariation::create([
                'moysklad_id' => $response['id'],
                'name' => $response['name'],
                'sale_price' => $salePrice,
                'product_id' => $productId
            ]);
        }

        $filterIds = [];
        $filterCategory = null;

        foreach ($response['characteristics'] as $characteristic) {
            $categoryId = $this->productCategoryIds[$productId];
            $filterCategory = FilterCategory::with('filters')->whereHas('productCategories', function ($q) use ($categoryId) {
                $q->where('id', $categoryId);
            })->where('alias', $characteristic['name'] . ' (' . $categoryId . ')')->first();


            if (!$filterCategory) {
                $filterCategory = FilterCategory::create([
                    'name' => $characteristic['name'],
                    'alias' => $characteristic['name'] . ' (' . $categoryId . ')']);
                $filterCategory->productCategories()->attach($this->productCategoryIds[$productId]);
            }

            $filter = Filter::where('name->ru', $characteristic['value'])->where('category_id', $filterCategory->id)->first();
            if (!$filter) {
                $filter = Filter::create(['name' => $characteristic['value'], 'category_id' => $filterCategory->id]);
            }
            $filterIds[] = $filter->id;
        }

        if (count($filterIds)) {
            $productVariation->filters()->sync($filterIds);
        }
    }


    //update

    private function updateCategory($category, $data)
    {
        if ($category->getTranslation('name', 'ru') != $data['name']) {
            $category->setTranslation('name', 'ru', $data['name']);
            $category->save();
        }

        if ($category->parent_id != $data['parent_id']) {
            $category->parent_id = $data['parent_id'];
            $category->save();
        }
    }

    private function updateProduct($product, $data)
    {
        $changed = false;
        if ($product->getTranslation('name', 'ru') != $data['name']) {
            $product->setTranslation('name', 'ru', $data['name']);
            $changed = true;
        }

        if ($product->category_id != $data['category_id']) {
            $product->category_id = $data['category_id'];
            $changed = true;
        }

        if ($product->user_id != $data['user_id']) {
            $product->user_id = $data['user_id'];
            $changed = true;
        }

        if ($product->external_code != $data['external_code']) {
            $product->external_code = $data['external_code'];
            $changed = true;
        }

        if ($changed) {
            $product->save();
        }
    }

    private function updateVariation($variation, $data)
    {
        $changed = false;

        if ($variation->getTranslation('name', 'ru') != $data['name']) {
            $variation->setTranslation('name', 'ru', $data['name']);
            $changed = true;
        }

        if ($variation->product_id != $data['product_id']) {
            $variation->product_id = $data['product_id'];
            $changed = true;
        }

        if ($variation->sale_price != $data['sale_price']) {
            $variation->sale_price = $data['sale_price'];
            $changed = true;
        }

        if ($variation->external_code != $data['external_code']) {
            $variation->external_code = $data['external_code'];
            $changed = true;
        }

        if ($changed) {
            $variation->save();
        }
    }


    //hooks

    public function deleteWebHooks($entity)
    {
        $message = 'not found';

        $size = 0;
        $offset = 0;
        $limit = 100;

        do {
            $webHooks = $this->getResponse($this->baseUrl . '/entity/webhook?offset=' . $offset . '&limit=' . $limit);
            foreach ($webHooks['rows'] as $webHook) {
                if ($webHook['entityType'] == $entity) {
                    $responseStatus = $this->deleteRequest($this->baseUrl . '/entity/webhook/' . $webHook['id']);
                    if ($responseStatus == 200) {
                        $message = 'Deleted : (' . \GuzzleHttp\json_encode($webHook, true) . ')';
                    }
                }
            }

            if (!$offset) {
                $size = $webHooks['meta']['size'];
            }

            $size -= $limit;
            $offset += $limit;
        } while ($size > 0);

        return $message;
    }

    public function createWebHooks($events)
    {
        foreach ($events as $event) {
            try {
                $responseStatus = $this->postRequest($this->baseUrl . '/entity/webhook', $event);
                if ($responseStatus == 200) {
                    dump('Created : (' . \GuzzleHttp\json_encode($event, true) . ')');
                }
            } catch (\Exception $exception) {
                dump('Ошибка ' . $exception->getCode() . ' ' . $event['entityType']);
            }
        }
    }

    public function handleWebHook(array $webHookResponse)
    {
        foreach ($webHookResponse['events'] as $event) {
            if ($event['action']) {
                if ($event['action'] == 'DELETE') {
                    try {
                        $urlExploded = explode('/', $event['meta']['href']);
                        $idFromUrl = array_pop($urlExploded);

                        switch ($event['meta']['type']) {
                            case 'productfolder':
                                $category = Category::where('moysklad_id', $idFromUrl)->first();
                                if ($category) $category->delete();
                                break;
                            case 'product':
                                $product = Product::where('moysklad_id', $idFromUrl)->first();
                                if ($product) $product->delete();

                                break;
                            case 'variant':
                                $variation = ProductVariation::where('moysklad_id', $idFromUrl)->first();
                                if ($variation) $variation->delete();
                                break;
                        }
                    } catch (\Exception $exception) {
                        info('----- event DELETE -------: ' . \GuzzleHttp\json_encode($event, true) . ' --- ' . $exception->getMessage());
                    }
                } else {
                    try {
                        $response = $this->getResponse($event['meta']['href']);
                        switch ($event['meta']['type']) {
                            case 'productfolder':
                                $this->createCategory($response);
                                break;
                            case 'product':
                                $this->createProduct($response);
                                break;
                            case 'variant':
                                $this->createVariation($response);
                                break;
                        }
                    } catch (\Exception $exception) {
                        info('------ event CREATE/UPDATE -------: ' . \GuzzleHttp\json_encode($event, true) . ' --- ' . $exception->getMessage());
                    }
                }
            }
        }
    }


    protected function getVariationByMoyskladId($url, $type)
    {
        $urlPath = parse_url($url);
        $url = $urlPath['path'];
        $urlExploded = explode('/', $url);
        $moyskladId = array_pop($urlExploded);

        $variation = null;
        if ($type == 'product') {
            $product = Product::with('variations')->where('moysklad_id', $moyskladId)->first();

            if ($product) {
                $variation = ($product->variations->count()) ? $product->variations->first() : null;
            }
        } else {
            $variation = ProductVariation::where('moysklad_id', $moyskladId)->first();
        }

        return $variation;
    }


    public function syncQuantityProductVariations()
    {
        $size = 0;
        $offset = 0;
        $limit = 1000;

        $result = ['notFound' => [], 'saved' => []];
        $variationIds = [];

        do {
            $stockProductVariants = $this->getResponse($this->baseUrl . '/report/stock/all?offset=' . $offset . '&limit=' . $limit);
            foreach ($stockProductVariants['rows'] as $productVariantRow) {
                $variation = $this->getVariationByMoyskladId($productVariantRow['meta']['href'], $productVariantRow['meta']['type']);

                if ($variation) {
                    $variationIds[] = $variation->id;
                    $variation->quantity = (int)$productVariantRow['quantity'];
                    $variation->save();
                    $result['saved'][] = $productVariantRow['meta']['href'];
                } else {
                    $result['notFound'][] = $productVariantRow['meta']['href'];
                }
            }

            if (!$offset) {
                $size = $stockProductVariants['meta']['size'];
            }

            $size -= $limit;
            $offset += $limit;

        } while ($size > 0);

        $variations = ProductVariation::whereNotIn('id', $variationIds)->where('quantity', '>', 0)->get();
        foreach ($variations as $variation) {
            $variation->quantity = 0;
            $variation->save();
        }

        return $result;
    }

}
