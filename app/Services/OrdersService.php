<?php


namespace App\Services;


use App\Order;
use App\Product;
use App\ProductVariation;

class OrdersService
{

    public function createOrder($basket)
    {
        $lastOrder = Order::orderBy('index', 'desc')->first();
        if ($lastOrder) {
            $index = $lastOrder->index + 1;
        } else {
            $index = 10000;
        }

        $promo = json_decode(request()->get('promoCode'), true);

        $order = Order::create([
            'index' => $index,
            'user_id' => auth()->id(),
            'name' => request()->get('name'),
            'surname' => request()->get('surname'),
            'phone' => request()->get('phone'),
            'city' => request()->get('city'),
            'address' => request()->get('address'),
            'promocode' => isset($promo['title']) ? $promo['title'] : null,
            'payment' => request()->get('payment'),
            'amount' => request()->get('amount'),
            'promocode_id' => isset($promo['id']) ? $promo['id'] : null,
            "present_receipt" => filter_var(request()->get('present_receipt'), FILTER_VALIDATE_BOOLEAN),
            "present_package" => filter_var(request()->get('present_package'), FILTER_VALIDATE_BOOLEAN),
            'present_package_price' => request()->get('present_package_price'),
            'present_text' => request()->get('present_text'),
            'reported_delivery_date' => new \DateTime(request()->get('reported_delivery_date')),
            'discount' => request()->get('totalDiscount'), // isset($promo['discount']) ? $promo['discount'] : null,
            'discount_type' => isset($promo['type_discount']) ? $promo['type_discount'] : null,
            'delivery_sum' => request()->get('delivery_sum'),
            'mq' => request()->get('mq'),
        ]);

        if ($order) {
            $variations = ProductVariation::with('discount')->whereIn('id', array_keys($basket))->get();
            $data = [];
            foreach ($variations as $variation) {
                $data[$variation->id]['quantity'] = $basket[$variation->id]['quantity'];
                $data[$variation->id]['sum'] = $variation->sale_price * $basket[$variation->id]['quantity'];
                if ($variation->discount){
                    $data[$variation->id]['sum'] = round($variation->sale_price * (1 - ($variation->discount->count/100)));
                }
            }

            $order->orderVariations()->sync($data);

            if (app()->environment() != 'local') {
                $items = [];
                foreach ($order->orderVariations()->get() as $item) {
                    $product = Product::find($item->product_id);
                    $items[] = [
                        'offer' => [
                            'externalId' => $item->external_code ? $product->external_code . '#' . $item->external_code : $product->external_code,
                            'xmlId' => $item->external_code ? $product->external_code . '#' . $item->external_code : $product->external_code,
                        ],
                        'quantity' => (float)$item->pivot->quantity,
                        'productName' => $product->name,
                        'discountManualAmount' => $product->name,
//                        'priceType' => ['code' => 'KGS'],
//                        'initialPrice' => $item->pivot->sum,
                    ];
                };
                $data = [
                    'number' => $order->index,
                    'firstName' => $order->name,
                    'phone' => $order->phone,
                    'email' => $order->email,
                    'discountManualAmount' => $order->discount,
//                    'discountManualPercent' => $order->discount,
                    'items' => $items,
                    'delivery' => [
                        'address' => [
                            'countryIso' => 'kg',
                            'city' => $order->city,
                            'street' => $order->address,
                        ],
                        'cost' => $order->delivery_sum,
                        'date' => $order->reported_delivery_date->format('Y-m-d')
                    ],
                ];

                $retailService = new RetailcrmService();
                $retailService->setParams(['order' => json_encode($data)]);
                $response = $retailService->postResponse('orders/create');
//            $retailService->setParams(['by' => 'id']);
//            $response = $retailService->getResponse('orders/158');
//            $response = $retailService->getResponse('store/product-groups');
//            $response = $retailService->getResponse('store/products');
//            $response = $retailService->getResponse('customers');
//            dump($response);
            }

        }

        return $order;
    }
}
