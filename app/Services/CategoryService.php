<?php


namespace App\Services;


use App\Category;
use App\FilterCategory;
use App\Filters\VariationFilters;
use App\Product;
use App\ProductVariation;
use Illuminate\Support\Facades\Cache;

class CategoryService
{
    private $breadCrumbs = [];
    private $currentUrl = [];


    public function getCategoryData($slug)
    {
        $this->currentUrl = explode('/', url()->current());
        $category = Category::where('slug', $slug)->first();
        if (!$category) {
            abort(404);
        }

        $categoryIds = Category::descendantsAndSelf($category->id)->pluck('id');


        $filters = [];
        if ($categoryIds->count() == 1) {
            $filters = FilterCategory::with('productCategories', 'filters')
                ->whereHas('productCategories', function ($q) use ($categoryIds) {
                    $q->whereIn('category_id', $categoryIds);
                })->get();
        }

        $maxPrice = ProductVariation::whereHas('product', function ($q) use ($categoryIds) {
            $q->whereIn('category_id', $categoryIds);
        })->max('sale_price');

        $variations = ProductVariation::with('discount', 'images', 'product.images', 'filters')
            ->whereHas('product', function ($q) use ($categoryIds) {
                $q->whereIn('category_id', $categoryIds);
            })
            ->where(function ($q) {
                $q->whereHas('images');
                $q->orWhereHas('product.images');
            })
            ->where('admin_confirm', true)
            ->where('quantity', '!=', 0);

        if (!Cache::has('productIdsOrderByCategoryId')) {
            $productIds = Product::pluck('category_id', 'id');
            Cache::put('productIdsOrderByCategoryId', array_keys($productIds->toArray()), now()->addDays(1));
        }
        $productIds = implode(', ', Cache::get('productIdsOrderByCategoryId'));

        $variations = $variations->filter(new VariationFilters)
            ->where('admin_confirm', true)
            ->orderByRaw('FIELD(product_id, '.$productIds.')')
            ->paginate(30);

        $this->breadCrumb(Cache::get('tree_categories'), '/catalog');

        $display = 'list';
        if (request()->has('display')) {
            $display = request()->get('display');
        }

        return [
            'display' => $display,
            'category' => $category,
            'variations' => $variations,
            'filters' => $filters,
            'maxPrice' => $maxPrice ?? 100000,
            'breadCrumbs' => $this->breadCrumbs
        ];
    }


    private function breadCrumb($categories, $slug = '')
    {
        foreach ($categories as $category) {
            if (in_array($category->slug, $this->currentUrl)) {
                $this->breadCrumbs[] = [
                    'name' => $category->name,
                    'path' => $slug . '/' . $category->slug,
                ];
                $this->breadCrumb($category->children, $slug . '/' . $category->slug);
            } else {
                continue;
            }

        }
    }
}
