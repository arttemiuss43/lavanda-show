<?php

namespace App\Filters;


class VariationFilters
{
    protected $builder;
    protected $filters = ['byFilters', 'bySort', 'byPrice'];

    public function apply($query)
    {
        $this->builder = $query;

        foreach ($this->filters as $filter) {
            $this->$filter();
        }

        return $this->builder;
    }

    protected function byFilters()
    {
        if (request()->has('filters')) {
            if (request()->get('filters')) {
                $filterIds = explode(',', request()->get('filters'));
                $this->builder->whereHas('filters', function ($q) use ($filterIds) {
                    $q->whereIn('filter_id', $filterIds);
                });
            }
        }
    }

    protected function bySort()
    {
        $sortValue = 'name';
        $sortType = 'asc';
        if (request()->has('sort')) {
            $sort = request()->get('sort');
            if ($sort) {
                $sort = explode('-', $sort);
                $sortValue = $sort[0];
                $sortType = $sort[1];
            }
        }

        $this->builder->orderBy($sortValue, $sortType);
    }

    protected function byPrice()
    {
        if (request()->has('min') && request()->has('max')) {
            $min = request()->get('min');
            $max = request()->get('max');
            if ($min && $max) {
                $this->builder->whereBetween('sale_price', [$min, $max]);
            } elseif ($min && !$max){
                $this->builder->where('sale_price', '>', $min);
            } elseif(!$min && $max) {
                $this->builder->where('sale_price', '<', $max);
            }
        }
    }
}