<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Translations\HasTranslations;

class Address extends Model
{
    use HasTranslations;
    protected $table = 'addresses';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'surname',
        'address',
        'city',
        'phone',
    ];

    public $translatable = ['city', 'address'];
}
