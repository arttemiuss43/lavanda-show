<?php

namespace App;

use App\Translations\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    use HasTranslations;

    protected $table = 'filters';

    protected $fillable = ['name', 'category_id'];

    public $translatable = ['name'];
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(FilterCategory::class, 'category_id');
    }
}
