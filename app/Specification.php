<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Translations\HasTranslations;

class Specification extends Model
{
    use HasTranslations;

    protected $table = 'specifications';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'value',
        'position',
        'product_id'
    ];

    public $translatable = ['name', 'value'];


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
