<?php

namespace App;

use App\Translations\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class AddressDelivery extends Model
{
    use HasTranslations;

    protected $fillable = [
        'title',
        'price',
        'limit',
        'days',
    ];

    public $translatable = ['title'];
}
