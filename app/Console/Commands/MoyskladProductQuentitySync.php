<?php

namespace App\Console\Commands;

use App\Services\MoyskladService;
use Illuminate\Console\Command;

class MoyskladProductQuentitySync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moysklad:quantity-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'moysklad quantity sync';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moyskladService = new MoyskladService();
        $moyskladService->getStarted();
        $result = $moyskladService->syncQuantityProductVariations();

        $this->info('updated: ' . count($result['saved']));

        if (count($result['notFound'])) {
            $this->error('not found: '. count($result['notFound']));
            info('Not found products for sync quantity: ', $result['notFound']);
        }
    }
}
