<?php

namespace App\Console\Commands;

use App\Services\MoyskladService;
use Illuminate\Console\Command;

class MoyskladWebHookCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moysklad:create-web-hooks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create moysklad web hook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('app.env') == 'local'){
            dd('error');
        }

        $webHookUrl = route('web.hook.handle');

        $events = [
            //productfolder
            [
                "url" => $webHookUrl,
                "action" => "CREATE",
                "entityType" => "productfolder"
            ],
            [
                "url" => $webHookUrl,
                "action" => "UPDATE",
                "entityType" => "productfolder"
            ],
            [
                "url" => $webHookUrl,
                "action" => "DELETE",
                "entityType" => "productfolder"
            ],

            //product
            [
                "url" => $webHookUrl,
                "action" => "CREATE",
                "entityType" => "product"
            ],
            [
                "url" => $webHookUrl,
                "action" => "UPDATE",
                "entityType" => "product"
            ],
            [
                "url" => $webHookUrl,
                "action" => "DELETE",
                "entityType" => "product"
            ],

            //variant
            [
                "url" => $webHookUrl,
                "action" => "CREATE",
                "entityType" => "variant"
            ],
            [
                "url" => $webHookUrl,
                "action" => "UPDATE",
                "entityType" => "variant"
            ],
            [
                "url" => $webHookUrl,
                "action" => "DELETE",
                "entityType" => "variant"
            ],

            // Отгрузки,
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "demand"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "demand"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "demand"
//            ],
//            //Приёмка
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "supply"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "supply"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "supply"
//            ],
//
//
//            //Заказ покупателя
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "customerorder"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "customerorder"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "customerorder"
//            ],
//
//            //Возврат покупателя
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "salesreturn"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "salesreturn"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "salesreturn"
//            ],
//
//            //Заказы поставщикам
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "purchaseorder"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "purchaseorder"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "purchaseorder"
//            ],
//
//            //Возвраты поставщикам
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "purchasereturn"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "purchasereturn"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "purchasereturn"
//            ],
//
//
//            //Оприходования
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "enter"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "enter"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "enter"
//            ],
//
//            //Списания
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "loss"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "loss"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "loss"
//            ],
//
//            //Перемещения
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "move"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "move"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "move"
//            ],
//
//            //Заказы на производство
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "processingorder"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "processingorder"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "processingorder"
//            ],
//
//            //Тех. операции
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "processing"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "processing"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "processing"
//            ],
//
//            //Розничные возвраты
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "retailsalesreturn"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "retailsalesreturn"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "retailsalesreturn"
//            ],
//
//            //Розничные продажи
//            [
//                "url" => $webHookUrl,
//                "action" => "CREATE",
//                "entityType" => "retaildemand"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "UPDATE",
//                "entityType" => "retaildemand"
//            ],
//            [
//                "url" => $webHookUrl,
//                "action" => "DELETE",
//                "entityType" => "retaildemand"
//            ],

        ];

        $moyskladService = new MoyskladService();
        $moyskladService->getStarted();

        $moyskladService->createWebHooks($events);
    }
}
