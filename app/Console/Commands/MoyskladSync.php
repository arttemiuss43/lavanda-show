<?php

namespace App\Console\Commands;

use App\Services\MoyskladService;
use http\Message;
use Illuminate\Console\Command;

class MoyskladSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moysklad:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'moysklad sync';


    public function handle()
    {
        $moyskladService = new MoyskladService();
        $bar = $this->output->createProgressBar(4);
        $bar->setFormat("Sync %current_step%...\n%current%/%max% %bar% %percent:3s%%");
        $this->currentStep($bar, 'started');
        $moyskladService->getStarted();
        $moyskladService->getOurDataForSync();

        $this->currentStep($bar, 'Users');
        $moyskladService->syncUsers();

        $this->currentStep($bar, 'Categories');
        $moyskladService->syncCategories();

        $this->currentStep($bar, 'Products');
        $moyskladService->syncProducts();

        $this->currentStep($bar, 'Product Variations');
        $moyskladService->syncProductVariants();

        $this->currentStep($bar, 'finished');
        $this->output->newLine(1);
        $this->info('DONE: Sync completed successfully.');
    }

    private function currentStep($bar, $message){
        $bar->setMessage($message, 'current_step');
        sleep(1);
        if ($message == 'started') {
            return $bar->start();
        }
        if ($message == 'finished') {
            return $bar->finish();
        }
        return $bar->advance();
    }
}
