<?php

namespace App\Console\Commands;

use App\Product;
use App\ProductVariation;
use Illuminate\Console\Command;

class GenerateArticlesForProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:articles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generating articles for products and variations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $products = Product::where('article', 0)->get();
        $products = Product::all();

        $bar = $this->output->createProgressBar(count($products));
        $bar->setFormat("%current%/%max% %bar% %percent:3s%% %elapsed:6s%/%estimated:-6s%");
        $bar->start();

        $article = 1;
        foreach ($products as $product) {
            $uniqArticle = false;
            while (!$uniqArticle):
                $article = rand(10000, 99999);
                $article = $article * 100 + 1;
                if (!Product::where('article', $article)->first()) {
                    $uniqArticle = true;
                }
            endwhile;

            $product->article = $article;
            $variations = ProductVariation::where('product_id', $product->id)->get();

            if ($variations) {
                foreach ($variations as $variation) {
                    $variation->article = $article;
                    $article ++;
                    $variation->save();
                }
            }

            $product->save();
            $bar->advance();
        }

        $bar->finish();
        $this->output->newLine(1);
        $this->info('DONE: Articles for products and variations generated successful!');
    }
}
