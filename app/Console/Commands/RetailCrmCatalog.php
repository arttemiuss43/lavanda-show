<?php

namespace App\Console\Commands;

use DateTime;
use Illuminate\Console\Command;
use Spatie\ArrayToXml\ArrayToXml;

class RetailCrmCatalog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retailCrm:generate-catalog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create catalog xml fail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $array = [
            'shop' => [
                'name' => 'Lavanda',
                'company' => 'Arniro',
                'categories' => [
                    'category' => []
                ],
                'offers' => [
                    'offer' => []
                ],
            ]
        ];

        $categories = \App\Category::get();

        foreach ($categories as $category) {
            $attributes = [];
            $attributes['id'] = $category->id;
            if ($category->parent_id) {
                $attributes['parentId'] = $category->parent_id;
            }
            $array['shop']['categories']['category'][] = [
                '_attributes' => $attributes,
                '_value' => $category->name,
            ];
        }

        $variations = \App\ProductVariation::with('filters.category', 'product.category')->get();

        foreach ($variations as $variation) {

            $params = [];
            $params[] = [
                '_attributes' => ['name' => 'Артикул', 'code' => 'article'],
                '_value' => (string)$variation->article,
            ];
            foreach ($variation->filters as $filter) {
                switch ($filter->category->name) {
                    case 'Размер':
                        $params[] = [
                            '_attributes' => ['name' => 'Размер', 'code' => 'size'],
                            '_value' => $filter->name,
                        ];
                        break;
                    case 'Цвет':
                        $params[] = [
                            '_attributes' => ['name' => 'Цвет', 'code' => 'color'],
                            '_value' => $filter->name,
                        ];
                        break;
                }
            }

            $array['shop']['offers']['offer'][] = [
                '_attributes' => [
                    'id' => $variation->id,
                    'productId' => $variation->product_id,
                    'quantity' => $variation->quantity
                ],
                'url' => route('catalog.product.variation', ['slug' => $variation->product->slug, 'article' => $variation->article]),
                'price' => $variation->sale_price,
                'categoryId' => $variation->product->category->id,
                'name' => $variation->name,
                'xmlId' => ($variation->moysklad_id) ? $variation->moysklad_id : $variation->product->moysklad_id,
                'productName' => $variation->name,
                'param' => $params,
            ];
        }


        $date = new DateTime();

        $result = ArrayToXml::convert($array, [
            'rootElementName' => 'yml_catalog',
            '_attributes' => [
                'date' => $date->format('Y-m-d H:i:s'),
            ],
        ], true, 'UTF-8');

        $file = storage_path() . '/app/public/retailCRM/catalog.xml';

        file_put_contents($file, $result);

        $this->info('ok');
    }
}
