<?php

namespace App\Console\Commands;

use App\Services\MoyskladService;
use Illuminate\Console\Command;

class MoyskladWebHookDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moysklad:delete-web-hooks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete moysklad web hook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('app.env') == 'local'){
            dd('error');
        }
        $moyskladService = new MoyskladService();
        $moyskladService->getStarted();

        $entity = $this->choice('Выберите сущнсть', [
            'productfolder',
            'product',
            'variant',
            'service',
            "demand",
            "supply",
            "customerorder",
            "salesreturn",
            "purchaseorder",
            "purchasereturn",
            "enter",
            "loss",
            "move",
            "processingorder",
            "processing",
            "retailsalesreturn",
            "retaildemand",
        ]);

        $res = $moyskladService->deleteWebHooks($entity);

        $this->info($res);
    }
}
