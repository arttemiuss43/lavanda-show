<?php


namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use App\Translations\HasTranslations;;


class FaqCategory extends Model
{
    use Sluggable, HasTranslations;

    protected $table = 'faq_categories';

    protected $fillable = [
        'name',
        'slug',
        'index',
    ];

    public $translatable = ['name'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'faq_category_id');
    }
}

