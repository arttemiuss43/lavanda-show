<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wish extends Model
{
    protected $fillable = [
        'product_variation_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
