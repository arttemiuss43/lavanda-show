<?php

namespace App;

use App\Address;
use App\Order;
use App\Wish;
use AppendIterator;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject

{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'moysklad_id', 'name', 'phone', 'email', 'password', 'type', 'type_registered', 'mq',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // public function wishes()
    // {
    //    return $this->hasMany('App\Wish');
    // }

//    public function addresses()
//    {
//        return $this->hasMany(Address::class);
//    }
//
//    public function orders()
//    {
//        return $this->hasMany(Order::class, 'user_id');
//    }
//
//    public function products()
//    {
//        return $this->hasMany(\Arniro\User::class);
//    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isAdmin()
    {
        if ($this->type == 'legal') return true;
        return false;
    }

    public function wishes() 
    {
        return $this->hasMany(Wish::class);
    }
}
