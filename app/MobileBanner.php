<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileBanner extends Model
{
    protected $table = 'mobile_banners';

    protected $fillable = ['title','image','link', 'index'];

}
