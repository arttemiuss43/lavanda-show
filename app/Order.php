<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'id',
        'index',
        'user_id',
        'name',
        'surname',
        'phone',
        'city',
        'address',
        'promocode',
        'status',
        'promocode_id',
        'promocode',
        'paid',
        'executor',
        'payment',
        'amount',
        'reported_delivery_date',
        'discount',
        'discount_type',
        'delivery_sum',
        'present_receipt',
        'present_package',
        'present_package_price',
        'present_text',
        'mq',
    ];

//    protected $casts = [
//        'reported_delivery_date' => 'datetime',
//    ];


    public function orderVariations()
    {
        return $this->belongsToMany(
            ProductVariation::class,
            'order_variations',
            'order_id',
            'variation_id'
        )->withPivot(['id','quantity', 'sum']);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
