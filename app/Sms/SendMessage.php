<?php


namespace App\Sms;


use GuzzleHttp\Client;
use Vyuldashev\XmlToArray\XmlToArray;


class SendMessage
{
    public $phone;
    public $message;

    public function __construct($phone, $message)
    {
        $this->phone = $phone;
        $this->message = $message;
    }

    public function handle(){

        $content = $this->generateXML();
        $result = $this->sendSms($content);

        return $result;
    }

    public function generateXML(){

        $test = env('APP_ENV') == 'local' ? '<test>1</test>' : '';
        $content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                        <message>
                        <login>" . config('smspro.login') . "</login>
                        <pwd>" . config('smspro.password') . "</pwd>
                        <id>" . uniqid() . "</id>
                        <sender>" . config('smspro.sender') . "</sender>
                        <text>" . $this->message . "</text>
                        <phones>
                        <phone>" . $this->phone . "</phone>
                        </phones>
                        " . $test . "
                        </message>";

        return $content;

    }

    public function sendSms($content){

        $client = new Client([ 'base_uri' => config('smspro.api') ]);

        $res = $client->request('POST', $url = null,
            [
                'headers'=>['Content-Type' => 'text/xml; charset=UTF8'],
                'body'=>$content
            ]
        )->getBody()->getContents();

        $arr = XmlToArray::convert($res);
        $status = $arr['response']['status'];

        info('SMS status: ' . $status);

        if (($status == 0) || ($status == 11)){
            return true;
        }

        return false;

    }
}
