<?php


namespace App\Mail;


use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class CallOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $phone;
    public $name;

    /**
     * Create a new message instance.
     *
     * @param $phone
     */
    public function __construct($phone, $name)
    {
        $this->phone = $phone;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.call')->subject("Заказ звонка");
    }
}
