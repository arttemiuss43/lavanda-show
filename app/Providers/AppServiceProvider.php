<?php

namespace App\Providers;

use App\Category;
use App\Image;
use App\Observers\ImageObserver;
use App\Observers\ProductObserver;
use App\Observers\ProductVariationObserver;
use App\Option;
use App\Product;
use App\ProductVariation;
use App\Services\CategoryService;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setCurrentLocale();
        Product::observe(ProductObserver::class);
        ProductVariation::observe(ProductVariationObserver::class);
        Image::observe(ImageObserver::class);

        if (!app()->runningInConsole()) {
            $this->setCacheData();
            View::composer(['layouts.master', 'layouts.private'], function ($views) {
                $this->setCacheData();
                $user = User::with('wishes')->find(auth()->id());
                $views->with('user', $user);
                $views->with('delivery', Option::pluck('value', 'key'));
            });
        }
    }


    protected function setCurrentLocale()
    {
        $newLocale = null;
        // if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        //     $localeAcceptExploded = explode("_", locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']));
        //     foreach ($localeAcceptExploded as $localeAccept) {
        //         foreach (config('app.locales') as $locale => $name) {
        //             if ($localeAccept == $locale) {
        //                 $newLocale = $locale;
        //                 break;
        //             }
        //         }
        //         if ($newLocale) break;
        //     }
        // }

        if (!$newLocale) $newLocale = 'ru';

        app()->setLocale($newLocale);
    }

    private function setCacheData(){

//        Cache::flush();
        if (!Cache::has('menu')) {
            $menu = Category::whereNull('parent_id')->where('show', 1)->get();
            Cache::put('menu', $menu, now()->addMinutes(5));
        }

        if (!Cache::has('tree_categories')) {
            $treeCategories = Category::where('show', 1)->get()->toTree();
            Cache::put('tree_categories', $treeCategories, now()->addMinutes(5));
        }
    }
}
