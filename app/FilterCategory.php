<?php

namespace App;

use App\Translations\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class FilterCategory extends Model
{
    use HasTranslations;

    protected $table = 'filter_categories';

    protected $fillable = ['name', 'alias'];

    public $translatable = ['name'];
    public $timestamps = false;


    public function filters()
    {
        return $this->hasMany(Filter::class, 'category_id');
    }

    public function productCategories() {
        return $this->belongsToMany(
            Category::class,
            'filters_categories',
            'filter_id',
            'category_id'
        );
    }
}
