<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Translations\HasTranslations;
use Laravel\Scout\Searchable;

class ProductVariation extends Model
{
    use HasTranslations;
    use Searchable;

    protected $appends = ['checked'];

    protected $table = 'product_variations';

    protected $fillable = [
        'name',
        'moysklad_id',
        'article',
        'quantity',
        'sale_price',
        'discount_price',
        'recommended',
        'admin_confirm',
        'show',
        'product_id',
        'external_code',
        'discount_id'
    ];

    public $translatable = ['name'];

    public function getCheckedAttribute()
    {
        return $checked = false;
    }

    public function searchableAs()
    {
        return 'variation_index';
    }

    public function toSearchableArray()
    {
//        $array = $this->toArray();

        // Customize array...
        return [
            'id' => $this->id,
            'name' => $this->name,
            'article' => $this->article,
        ];
    }
//    }
//    public function getScoutKey()
//    {
//        return $this->article;
//    }
    public function setShowAttribute($value)
    {
        $this->attributes['show'] = !!$value;
    }

    public function setRecommendedAttribute($value)
    {
        $this->attributes['recommended'] = !!$value;
    }

    public function setAdminConfirmAttribute($value)
    {
        $this->attributes['admin_confirm'] = !!$value;
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }

    public function filters()
    {
        return $this->belongsToMany(
            Filter::class,
            'filter_variations',
            'variation_id',
            'filter_id'
        );
    }

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'variation_id');
    }

    public function adminConfirmedReviews()
    {
        return $this->hasMany(Review::class, 'variation_id')->where('admin_confirm', 1);
    }

    public function userReview()
    {
        return $this->hasOne(Review::class, 'variation_id')->where('user_id', auth()->id());
    }

    public function category()
    {
        return $this->product->category;
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function approve()
    {
        $this->admin_confirm = true;
        $this->save();
    }

    public function withProductImages()
    {
        if($this->images->count()){
            $images = $this->images;
        } elseif ($this->product->images->count()){
            $images = $this->product->images;
        }
        else {
            $images = [];
        }

        unset($this->images);

        $this->images = $images;

        return $this;
    }

    public function getSalePriceAttribute()
    {
        return (int) $this->attributes['sale_price'];
    }

    public function getDiscountPriceAttribute()
    {
        return (int) $this->attributes['discount_price'];
    }
}
