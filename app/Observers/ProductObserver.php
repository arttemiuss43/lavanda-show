<?php

namespace App\Observers;

use App\Product;
use Illuminate\Support\Facades\DB;

class ProductObserver
{
    public function deleting(Product $product)
    {
        foreach ($product->images as $image){
            $file = storage_path() . '/app/public/images/' . $image->getOriginal('src');

            if (file_exists($file)) {
                @unlink($file);
            }
        }


        $product->images()->delete();

        $where['imageable_id'] = $product->id;
        $where['imageable_type'] = Product::class;
        DB::table('imageables')->where($where)->delete();
    }

    public function creating(Product $product)
    {

        $article = 1;

        $uniqArticle = false;

        while (!$uniqArticle):
            $article = rand(10000, 99999);
            $article = $article * 100 + 1;
            if (!$product->where('article', $article)->first()) {
                $uniqArticle = true;
            }
        endwhile;

        $product->article = $article;
    }
}
