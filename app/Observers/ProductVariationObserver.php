<?php

namespace App\Observers;

use App\Product;
use App\ProductVariation;

class ProductVariationObserver
{
    public function creating(ProductVariation $productVariation)
    {
        $variations = ProductVariation::where('product_id', $productVariation->product_id)->get()->all();
        $product = Product::where('id',$productVariation->product_id)->first();

        $maxArticle = $product->article;
        if ($variations) {
            foreach ($variations as $variation) {
                if ($variation->article > $maxArticle) $maxArticle = $variation->article;
            }
            $maxArticle++;
        }
        $productVariation->article = $maxArticle;
    }

}
