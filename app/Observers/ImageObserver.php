<?php

namespace App\Observers;

use App\Image;
use App\Product;
use App\ProductVariation;
use Illuminate\Support\Facades\DB;

class ImageObserver
{
    /**
     * Handle the image "created" event.
     *
     * @param  \App\Image $image
     * @return void
     */
    public function created(Image $image)
    {
        //
    }

    /**
     * Handle the image "updated" event.
     *
     * @param  \App\Image $image
     * @return void
     */
    public function updated(Image $image)
    {
        //
    }

    /**
     * Handle the image "deleted" event.
     *
     * @param  \App\Image $image
     * @return void
     */
    public function deleted(Image $image)
    {
        DB::table('imageables')->where('image_id', $image->id)->delete();

        $file = storage_path() . '/app/public/images/' . $image->getOriginal('src');

        if (file_exists($file)) {
            @unlink($file);
        }
    }

    /**
     * Handle the image "restored" event.
     *
     * @param  \App\Image $image
     * @return void
     */
    public function restored(Image $image)
    {
        //
    }

    /**
     * Handle the image "force deleted" event.
     *
     * @param  \App\Image $image
     * @return void
     */
    public function forceDeleted(Image $image)
    {
        //
    }
}
