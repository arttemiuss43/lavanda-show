<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'src',
        'main_image',
        'show'
    ];

    public function setSrcAttribute($file)
    {
        if (is_object($file) && $file->isValid()) {
            $imageName = 'image_' . uniqid() . '.' . $file->guessClientExtension();
            $uploadPath = storage_path() . '/app/public/images';
            $file->getClientOriginalExtension();
            $file->move($uploadPath, $imageName);
            $this->attributes['src'] = $imageName;
        }
    }

    public function setShowAttribute($value)
    {
        $this->attributes['show'] = !!$value;
    }

    public function setMainImageAttribute($value)
    {
        $this->attributes['main_image'] = !!$value;
    }


    public function getSrcAttribute($value)
    {
        if ($value) {
            return asset('storage/images/' . $value);
        }
    }

    public function categories()
    {
        return $this->morphedByMany(Category::class, 'imageable');
    }

    public function products()
    {
        return $this->morphedByMany(Product::class, 'imageable');
    }
    public function variations()
    {
        return $this->morphedByMany(ProductVariation::class, 'imageable');
    }
}
