<?php

namespace App\Http\Resources;

use App\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class VariationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = collect(parent::toArray($request))->map(function ($value, $key) {
            return $this->$key;
        })->toArray();

        return array_merge($data, [
            'name' => $this->name,
            'slug' => $this->product->slug,
            'article' => rand(10000, 99999),
            'quantity' => 1,
            'salePrice' => $this->sale_price,
            'discountPrice' => $this->discount_price,
            'recommended' => $this->recommended,
            'productId' => $this->product_id,
            'images' => $this->images,
            'productName' => $this->product->name,
            'categoryId' => $this->product->category_id,
            'categoryIds' => Category::ancestorsAndSelf($this->product->category_id)->pluck('id')->toJson()
        ]);
    }
}
