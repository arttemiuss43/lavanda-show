<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class GetUser {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hasToken = true;

        try {
            JWTAuth::parseToken();
            $token = JWTAuth::getToken();
        } catch (JWTException $e) {
            $hasToken = false;
            $token = false;
        }

        if ($token) {
            // Try to verify token
            try {
                // If successful, save user on request
                $request->user = JWTAuth::authenticate($token);
            } catch (TokenBlacklistedException $e) {
                abort(401, 'Token "' . JWTAuth::manager()->decode($token, false)['jti'] . '" Blacklisted');
            } // If token has expired...
            catch (TokenExpiredException $e) {

                try {
                    // Try to refresh token
                    $token = JWTAuth::refresh($token);
                    JWTAuth::setToken($token);

                    // Authenticate with new token, save user on request
                    $request->user = JWTAuth::authenticate($token);
                } // If token refresh period has expired...
                catch (TokenExpiredException $e) {
                    $hasToken = false;
                }
            }
        }

        if (!$hasToken) {
            $request->user = false;

            return $next($request);
        }

        $response = $next($request);
        $response->headers->set('Authorization', 'Baerer ' . $token);

        return $response;
    }
}
