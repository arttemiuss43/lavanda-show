<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\AddressDelivery as AddressDeliveryResource;
use App\AddressDelivery;
use Arniro\Admin\Notifications\Toast;

class AddressesController extends Controller
{
    public function index()
    {
        return AddressDeliveryResource::collection(AddressDelivery::all())->get();
    }

    public function show(AddressDelivery $addressDelivery)
    {
        return AddressDeliveryResource::make($addressDelivery)->get();
    }

    public function create()
    {
        return AddressDeliveryResource::make(new AddressDelivery)->get();
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'price' => 'required|integer',
            'limit' => 'required|integer',
            'days' => 'required|integer',
        ]);

        return AddressDelivery::create(request()->all());
    }

    public function edit(AddressDelivery $addressDelivery)
    {
        return AddressDeliveryResource::make($addressDelivery)->get();
    }

    public function update(AddressDelivery $addressDelivery)
    {
        request()->validate([
            'title' => 'required',
            'days' => 'required',
        ]);

        return tap($addressDelivery)->update(request()->all());
    }
}
