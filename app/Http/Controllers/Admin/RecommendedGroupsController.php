<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\RecommendedGroup as RecommendedGroupResource;
use App\RecommendedGroup;

class RecommendedGroupsController extends Controller
{
    public function index()
    {
        return RecommendedGroupResource::fetch();
    }

    public function show(RecommendedGroup $recommendedGroup)
    {
        return RecommendedGroupResource::make($recommendedGroup)->get();
    }

    public function edit(RecommendedGroup $recommendedGroup)
    {
        return RecommendedGroupResource::make($recommendedGroup)->get();
    }

    public function update(RecommendedGroup $recommendedGroup)
    {
        $recommendedGroup->name = request()->input('name');
        $recommendedGroup->category_id = request()->input('category_id');
        $recommendedGroup->status = !! request()->input('status');

        return tap($recommendedGroup)->update(
            request()->validate([
                'name' => 'required',
                'category_id' => 'required',
            ])
        );

    }

    public function create()
    {
        return RecommendedGroupResource::make(new RecommendedGroup)->get();
    }

    public function store()
    {
        request()->validate([
            'name' => 'required',
            'category_id' => 'required',
        ]);

        $recommendedGroup = new RecommendedGroup();

        $recommendedGroup->name = request()->input('name');
        $recommendedGroup->category_id = request()->input('category_id');
        $recommendedGroup->status = !! request()->input('status');
        $recommendedGroup->save();
        return $recommendedGroup;
    }
}
