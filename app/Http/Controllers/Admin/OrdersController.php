<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Order as OrderResource;
use Arniro\Admin\Http\Controllers\Controller;
use App\Order;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Order::class, 'order');
    }

    public function index()
    {
        return OrderResource::fetch();
    }

    public function show(Order $order)
    {
        return OrderResource::make($order)->get();
    }

    public function edit(Order $order)
    {
        return OrderResource::make($order)->get();
    }

    public function update(Order $order)
    {
        if (!!$order->paid){
            abort_unless(request()->get('paid'), 502, 'Заказ был оплачен');
        }

        request()->merge([
            'paid' => 1
        ]);
        request()->validate([
            'index' => 'required',
            'user_id' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'address' => 'required',
            'status' => 'required',
            'payment' => 'required',
            'amount' => 'required',
            'reported_delivery_date' => 'required',
            'discount' => 'required',
            'discount_type' => 'required',
            'delivery_sum' => 'required',
        ]);
        return tap($order)->update(request()->all());
    }

    public function create()
    {
        return OrderResource::make(new Order)->get();
    }
//
//    public function store()
//    {
//        return Order::create(
//            request()->validate([
//                'id' => 'required',
//				'index' => 'required',
//				'user_id' => 'required',
//				'name' => 'required',
//				'surname' => 'required',
//				'phone' => 'required',
//				'city' => 'required',
//				'address' => 'required',
//				'status' => 'required',
//				'promocode_id' => 'required',
//				'promocode' => 'required',
//				'paid' => 'required',
//				'executor' => 'required',
//				'payment' => 'required',
//				'amount' => 'required',
//				'reported_delivery_date' => 'required',
//				'discount' => 'required',
//				'discount_type' => 'required',
//				'delivery_sum' => 'required',
//				'present_receipt' => 'required',
//				'present_package' => 'required',
//				'present_package_price' => 'required',
//				'present_text' => 'required',
//				'mq' => 'required'
//            ])
//        );
//    }
}
