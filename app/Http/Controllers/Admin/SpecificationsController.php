<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Specification as SpecificationsResource;
use Arniro\Admin\Http\Controllers\Controller;
use App\Specification;

class SpecificationsController extends Controller
{
    public function index()
    {
        return SpecificationsResource::collection(Specification::get())->get();
    }

    public function show(Specification $specification)
    {
        return SpecificationsResource::make($specification)->get();
    }

    public function edit(Specification $specification)
    {
        return SpecificationsResource::make($specification)->get();
    }

    public function update(Specification $specification)
    {
        request()->validate([
            'name.ru' => 'required',
            'value.ru' => 'required',
            'position' => 'nullable|integer'
        ]);
        return tap($specification)->update(request()->all());
    }

    public function create()
    {
        return SpecificationsResource::make(new Specification)->get();
    }

    public function store()
    {

        if (request()->get('viaResourceId') && request()->get('viaResource')) {
            request()->merge([
                'product_id' => request()->get('viaResourceId')
            ]);
        }

        request()->validate([
            'name.ru' => 'required',
            'value.ru' => 'required',
            'position' => 'nullable|integer',
            'product_id' => 'required',
        ]);

        $specificationLastPosition = Specification::where('product_id', request()->get('product_id'))
            ->orderBy('position', 'desc')
            ->first();

        request()->merge([
            'position' => ($specificationLastPosition) ? 1 + $specificationLastPosition->position : 1
        ]);

        return Specification::create(request()->all());
    }

}
