<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Variation as VariationResource;
use Arniro\Admin\Http\Controllers\Controller;
use App\ProductVariation;

class VariationsController extends Controller
{
    public function index()
    {
        return VariationResource::collection(ProductVariation::paginate(200))->get();
    }

    public function show($variation)
    {
        $variation = ProductVariation::with('filters.category')->find($variation);

        return VariationResource::make($variation)->get();
    }

    public function edit(ProductVariation $variation)
    {
        return VariationResource::make($variation)->get();
    }

    public function update(ProductVariation $variation)
    {
        request()->validate([
            'name.ru' => 'required',
            'quantity' => 'required|integer',
            'sale_price' => 'required|numeric',
            'product_id' => 'required|integer',
        ]);
        return tap($variation)->update(request()->all());
    }
}
