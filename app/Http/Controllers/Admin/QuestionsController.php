<?php

namespace App\Http\Controllers\Admin;

use App\FaqCategory;
use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\Question as QuestionResource;
use App\Question;

class QuestionsController extends Controller
{
    public function index()
    {
        return QuestionResource::collection(Question::latest()->get())->get();
    }

    public function show(Question $question)
    {
        return QuestionResource::make($question)->get();
    }

    public function edit(Question $question)
    {
        return QuestionResource::make($question)->get();
    }

    public function update(Question $question)
    {
        return tap($question)->update(
            request()->validate([
                'name' => 'required',
                'answer' => 'required',
                'faq_category_id' => 'required',
            ])
        );
    }

    public function create()
    {
        return QuestionResource::make(new Question)->get();
    }

    public function store()
    {
        $data = request()->validate([
            'faq_category_id' => 'required',
            'name' => 'required',
            'answer' => 'required',
        ]);

        $data['index'] = Question::max('index')+1;

        return Question::create($data);
    }
}
