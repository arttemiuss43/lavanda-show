<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Product as ProductResource;
use Arniro\Admin\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\Services\ImageService;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        return ProductResource::collection(
            Product::with('category', 'variations', 'images', 'specifications')
                ->orderBy('id', 'desc')
                ->paginate(200)
        )->get();
    }

    public function show(Product $product)
    {
        return ProductResource::make($product)->get();
    }

    public function edit(Product $product)
    {
        return ProductResource::make($product)->get();
    }

    public function update(Product $product)
    {
        request()->validate([
            'name.ru' => 'required',
        ]);

        return tap($product)->update(request()->all());
    }
}
