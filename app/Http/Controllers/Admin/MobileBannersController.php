<?php

namespace App\Http\Controllers\Admin;

use App\MobileBanner;
use Arniro\Admin\File;
use Arniro\Admin\Image;
use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\MobileBanner as MobileBannerResource;


class MobileBannersController extends Controller
{
    public function index()
    {
        return MobileBannerResource::collection(MobileBanner::latest()->get())->get();
    }

    public function show(MobileBanner $mobile_banner)
    {
        return MobileBannerResource::make($mobile_banner)->get();
    }

    public function edit(MobileBanner $mobile_banner)
    {
        return MobileBannerResource::make($mobile_banner)->get();
    }

    public function update(MobileBanner $mobile_banner, File $files)
    {
        $data = request()->validate([
            'title' => 'nullable',
            'image.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'link' => 'required',
        ]);

        if (request()->file('image')) {
            $data['image'] = $files->replace($data->image)->with('mobile_banners', request('image'));
        }

        return tap($mobile_banner)->update($data);
    }

    public function create()
    {
        return MobileBannerResource::make(new MobileBanner)->get();
    }

    public function store(File $files)
    {
        $data = request()->validate([
            'title' => 'nullable',
            'image' => 'required',
            'link' => 'required',
        ]);
        $data['index'] = MobileBanner::max('index')+1;
        $data['image'] = $files->store('mobile_banners', $data['image']);
        return MobileBanner::create($data);
    }
}
