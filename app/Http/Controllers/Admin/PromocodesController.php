<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\Promocode as PromocodeResource;
use App\Promocode;

class PromocodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PromocodeResource::collection(Promocode::all())->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return PromocodeResource::make(new Promocode)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'type_promo' => 'required',
            'delivery' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'switch' => 'nullable'
        ]);

        return Promocode::create(request()->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Promocode $promocode)
    {
        return PromocodeResource::make($promocode)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Promocode $promocode)
    {
        return PromocodeResource::make($promocode)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($promocode)
    {
        request()->validate([
            'title' => 'required',
            'type_promo' => 'required',
            'delivery' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'switch' => 'nullable'
        ]);

        $promocode = Promocode::find($promocode);

        return tap($promocode)->update(request()->except(['parent_id']));
    }
}
