<?php

namespace App\Http\Controllers\Admin;
use App\Admin\Question;
use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\FaqCategory as FaqCategoryResource;
use App\FaqCategory;

class FaqController extends Controller
{
    public function index()
    {
        return FaqCategoryResource::collection(FaqCategory::with('questions')->latest()->get())->get();
    }

    public function show(FaqCategory $faqCategory)
    {
        return FaqCategoryResource::make($faqCategory)->get();
    }

    public function edit($faqCategory)
    {
        return FaqCategoryResource::make($faqCategory)->get();
    }

    public function update(FaqCategory $faqCategory)
    {
        return tap($faqCategory)->update(request()->validate(['name' => 'required']));
    }

    public function create()
    {
        return FaqCategoryResource::make(new FaqCategory)->get();
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required',
        ]);
        $data['index'] = FaqCategory::max('index')+1;

        return FaqCategory::create($data);
    }
}
