<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\Category as CategoryResource;
use App\Category;
use Illuminate\Support\Facades\Cache;

class CategoriesController extends Controller
{
    public function index()
    {
        return response()->json([
           'categories' => \App\Category::get()->toTree()->toArray()
        ]);
    }

    public function show($categoryId)
    {
        $category = Category::with('filterCategories.filters')->find($categoryId);
        return CategoryResource::make($category)->get();
    }

    public function edit($categoryId)
    {
        $category = Category::find($categoryId);
        return CategoryResource::make($category)->get();
    }

    public function update($categoryId)
    {
        request()->validate([
            'name.ru' => 'required',
        ]);
        $category = Category::find($categoryId);

        Cache::flush();

        return tap($category)->update(request()->except(['parent_id']));
    }
}
