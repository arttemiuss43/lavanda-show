<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use Arniro\Admin\File;
use Arniro\Admin\Image;
use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\Banner as BannerResource;
use Illuminate\Support\Facades\Storage;


class BannersController extends Controller
{
    public function index()
    {
        return BannerResource::collection(Banner::latest()->get())->get();
    }

    public function show(Banner $banner)
    {
        return BannerResource::make($banner)->get();
    }

    public function edit(Banner $banner)
    {

        return BannerResource::make($banner)->get();
    }

    /**
     * @param Banner $banner
     * @param Image $image
     * @return mixed
     */
    public function update(Banner $banner, File $files)
    {
        $data = request()->validate([
            'title' => 'nullable',
            'des' => 'nullable',
            'image.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:8184',
            'link' => 'required',
        ]);

        if (request()->file('image')) {
            $data['image'] = $files->replace($banner->image)->with('banners', request('image'));
        }

        return tap($banner)->update($data);
    }

    public function create()
    {
        return BannerResource::make(new Banner)->get();
    }

    public function store(File $files)
    {
        $data = request()->validate([
            'title' => 'nullable',
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8184',
            'des' => 'nullable',
            'link' => 'required',
        ]);
        $data['index'] = 1 ; //Banner::max('index')+1;
        $data['image'] = $files->store('banners', request()->file('image'));

        return Banner::create($data);
    }
}
