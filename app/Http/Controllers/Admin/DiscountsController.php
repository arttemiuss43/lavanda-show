<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\Discount as DiscountResource;
use App\Discount;
use App\Category;
use App\Product;
use App\ProductVariation;

class DiscountsController extends Controller
{
    public function index()
    {
        return DiscountResource::fetch();
    }

    public function show(Discount $discount)
    {
        return DiscountResource::make($discount)->get();
    }

    public function edit(Discount $discount)
    {
        return DiscountResource::make($discount)->get();
    }

    public function update(Discount $discount)
    {
        request()->validate([
            'name' => 'required',
            'count' => 'required',
            'start' => 'required',
            'end' => 'required',
        ]);

        $discount->name = request()->input('name');
        $discount->count = request()->input('count');
        $discount->start = request()->input('start');
        $discount->end = request()->input('end');
        $discount->save();

        if(request()->input('productsWithDiscount') == NULL) {
            ProductVariation::where('discount_id', $discount->id)->update(['discount_id' => NULL]);
            return;
        }

        $productsId = explode(',', request()->input('productsWithDiscount'));

        ProductVariation::where('discount_id', $discount->id)->update(['discount_id' => NULL]);

        ProductVariation::whereIn('id', $productsId)->update(['discount_id' => $discount->id]);
    }

    public function create()
    {
        return DiscountResource::make(new Discount)->get();
    }

    public function store()
    {
        request()->validate([
            'name' => 'required',
            'count' => 'required',
            'productsWithDiscount' => 'required',
            'start' => 'required',
            'end' => 'required'
        ]);

        $productsId = explode(',', request()->input('productsWithDiscount'));

        $discount = new Discount();

        $discount->name = request()->input('name');
        $discount->count = request()->input('count');
        $discount->start = request()->input('start');
        $discount->end = request()->input('end');
        $discount->save();

        ProductVariation::whereIn('id', $productsId)->update(['discount_id' => $discount->id]);
    }

    public function destroy()
    {
        request()->validate([
            'id' => 'required'
        ]);

        ProductVariation::where('discount_id', request()->id)->update(['discount_id' => NULL]);

        Discount::destroy(request()->id);

        return Discount::with('variations')->get();
    }

    public function productsCategories()
    {
        $categoryes = Category::descendantsAndSelf(request()->id)->pluck('id');

        $products = Product::whereIn('category_id', $categoryes)->pluck('id')->toArray();

        $productVariations = ProductVariation::whereIn('product_id', $products)
            ->whereNotIn('id', request()->selectedProducts)
            ->where('discount_id', NULL)
            ->where('quantity', '>', 0)
            ->get(['id', 'name', 'sale_price']);

        return ['productVariations' => $productVariations,
                'categoryes' => $categoryes];
    }

    public function searchVariations()
    {
        $productVariations = ProductVariation::search(request()->input('search'))
            ->where('discount_id', NULL)
            ->orderBy('name')
            ->get();


        $productVariations = $productVariations->reject(function($value) {
            foreach(request()->input('selectedProducts') as $select) {
                return $select == $value->id;
            }
        });

        return ['productVariations' => $productVariations];
    }

    public function detachVariation()
    {
        request()->validate([
            'id' => 'required',
        ]);

        ProductVariation::where('id', request()->id)->update(['discount_id' => NULL]);
    }
}
