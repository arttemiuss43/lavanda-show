<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\ProductVariation as ProductVariationResource;
use App\ProductVariation;

class ProductVariationsController extends Controller
{
    public function index()
    {
        return ProductVariationResource::collection(ProductVariation::where('admin_confirm', false)->orderBy('product_id')->paginate(150))->get();
    }

    public function show(ProductVariation $productVariation)
    {
        return ProductVariationResource::make($productVariation)->get();
    }

    public function edit(ProductVariation $productVariation)
    {
        return ProductVariationResource::make($productVariation)->get();
    }

    public function update(ProductVariation $productVariation)
    {
        return tap($productVariation)->update(
            request()->all()
        );
    }

    public function create()
    {
        return ProductVariationResource::make(new ProductVariation)->get();
    }

    public function store()
    {
        return ProductVariation::create(
            request()->validate([
                'name' => 'required',
				'article' => 'required',
				'quantity' => 'required',
				'sale_price' => 'required',
				'discount_price' => 'required',
				'recommended' => 'required',
				'admin_confirm' => 'required',
				'show' => 'required'
            ])
        );
    }
}
