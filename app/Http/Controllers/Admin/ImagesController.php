<?php

namespace App\Http\Controllers\Admin;

use App\Admin\Image as ImageResource;
use App\Category;
use Arniro\Admin\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\ProductVariation;
use Illuminate\Support\Facades\DB;


class ImagesController extends Controller
{
    public function create()
    {
        $image = Image::create([
            'src' => request()->file('image'),
        ]);

        $model = null;
        switch (request()->get('imageable_type')) {
            case 'categories':
                $model = Category::find(request('imageable_id'));
                $model->images()->delete();
                break;
            case 'products':
                $model = Product::find(request('imageable_id'));
                break;
            case 'variations':
                $model = ProductVariation::find(request('imageable_id'));
                $product = Product::find($model->product_id);
                $product->images()->save($image);
                break;
        }

        if ($model) {
            $model->images()->save($image);
            return response()->json($model->images);
        } else {
            return false;
        }
    }


    public function attach($id)
    {
        $image = Image::find($id);
        $model = null;
        switch (request()->get('imageable_type')) {
            case 'products':
                $model = Product::find(request('imageable_id'));
                break;
            case 'variations':
                $model = ProductVariation::find(request('imageable_id'));
                break;
        }

        if ($model) {
            $model->images()->save($image);
            return response()->json($image);

        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $where = [];
        $where['image_id'] = $id;
        $where['imageable_id'] = (int)request()->get('imageable_id');

        switch (request()->get('imageable_type')) {
            case 'categories':
                $where['imageable_type'] = Category::class;
                break;
            case 'products':
                $where['imageable_type'] = Product::class;
                break;
            case 'variations':
                $where['imageable_type'] = ProductVariation::class;
                break;
        }

        DB::table('imageables')->where($where)->delete();

        if (request()->get('imageable_type') != 'variations') {
            Image::find($id)->delete();
        }
        return $id;
    }
}
