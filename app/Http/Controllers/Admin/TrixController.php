<?php


namespace App\Http\Controllers\Admin;


use Arniro\Admin\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TrixController extends  Controller
{
    private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    private $audio_ext = ['mp3', 'ogg', 'mpga'];
    private $video_ext = ['mp4', 'mpeg'];
    private $document_ext = ['doc', 'docx', 'pdf', 'odt'];

    public function store(Request $request)
    {
        $max_size = (int)ini_get('upload_max_filesize') * 1000;
        $all_ext = implode(',', $this->allExtensions());

//        $this->validate($request, [
//            'name' => 'required|unique:files',
//            'file' => 'required|file|mimes:' . $all_ext . '|max:' . $max_size
//        ]);
//
//        $model = new File();
//          dd($request['file']);
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $filename = $file->getClientOriginalName(); //!Full filename with extension!
        $type = $this->getType($ext);
        $storage_dir = 'attachments/'.(($type) ? $type : 'other') . 's';
//        $url = 'storage/' . $storage_dir . $filename;
//        Storage::putFileAs($storage_dir, $file, $filename);
        $url = $file->store($storage_dir, 'public');
        $url = url('/').'/storage/'.$url;
        return  response()->json(['url' => $url]);
    }

    private function allExtensions()
    {
        return array_merge($this->image_ext, $this->audio_ext, $this->video_ext, $this->document_ext);
    }

    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'image';
        }

        if (in_array($ext, $this->audio_ext)) {
            return 'audio';
        }

        if (in_array($ext, $this->video_ext)) {
            return 'video';
        }

        if (in_array($ext, $this->document_ext)) {
            return 'document';
        }
    }
}
