<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\BottomBanner as BottomBannerResource;
use App\AdvertisingBanner;
use Arniro\Admin\File;

class BottomBannersController extends Controller
{
    public function index()
    {
        return BottomBannerResource::fetch();
    }

    public function show(AdvertisingBanner $bottomBanner)
    {
        return BottomBannerResource::make($bottomBanner)->get();
    }

    public function edit(AdvertisingBanner $bottomBanner)
    {
        return BottomBannerResource::make($bottomBanner)->get();
    }

    public function update(AdvertisingBanner $bottomBanner, File $files)
    {
        $data = request()->validate([
            'title' => 'required',
            'link'  => 'required'
        ]);

        if (request()->file('mobile_image')) {
            $data['image'] = $files->replace($bottomBanner->image)->with('banners', request('mobile_image'));
        }
        if (request()->file('mobile_image')) {
            $data['image'] = $files->replace($bottomBanner->image)->with('banners', request('desktop_image'));
        }

        return tap($bottomBanner)->update($data);
    }

    public function create()
    {
        return BottomBannerResource::make(new AdvertisingBanner)->get();
    }

    public function store(File $files)
    {
        $data = request()->validate([
            'title' => 'required',
            'mobile_image' => 'required',
            'desktop_image' => 'required',
            'link'  => 'required'
        ]);

        $data['mobile_image'] = $files->store('banners', request('mobile_image'));
        $data['desktop_image'] = $files->store('banners', request('desktop_image'));

        return AdvertisingBanner::create($data);
    }
}
