<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\Review as ReviewResource;
use App\Review;

class ReviewsController extends Controller
{
    public function index()
    {
        return ReviewResource::fetch();
    }

    public function show(Review $review)
    {
        return ReviewResource::make($review)->get();
    }

    public function edit(Review $review)
    {
        return ReviewResource::make($review)->get();
    }

    public function update(Review $review)
    {
        request()->merge([
            'admin_confirm' => filter_var(request()->get('admin_confirm'), FILTER_VALIDATE_BOOLEAN)
        ]);

        return tap($review)->update(
            request()->all()
        );
    }

//    public function create()
//    {
//        return ReviewResource::make(new Review)->get();
//    }
//
//    public function store()
//    {
//        return Review::create(
//            request()->validate([
//                //
//            ])
//        );
//    }
}
