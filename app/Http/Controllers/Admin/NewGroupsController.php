<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\NewGroup as NewGroupResource;
use App\NewGroup;

class NewGroupsController extends Controller
{
    public function index()
    {
        return NewGroupResource::fetch();
    }

    public function show(NewGroup $newGroup)
    {
        return NewGroupResource::make($newGroup)->get();
    }

    public function edit(NewGroup $newGroup)
    {
        return NewGroupResource::make($newGroup)->get();
    }

    public function update(NewGroup $newGroup)
    {
        if (request()->input('status') == "true") {
            NewGroup::where('id', '!=', $newGroup->id)->update(['status' => 0]);
        }

        $newGroup->name = request()->input('name');
        $newGroup->category_id = request()->input('category_id');
        $newGroup->status = $this->checkStatus();

        return tap($newGroup)->update(
            request()->validate([
                'name' => 'required',
                'category_id' => 'required',
            ])
        );
    }

    public function create()
    {
        return NewGroupResource::make(new NewGroup)->get();
    }

    public function store()
    {
        request()->validate([
            'name' => 'required',
            'category_id' => 'required|integer',
        ]);

        if($this->checkStatus() == 1) {
            NewGroup::where('status', '!=', null)
                ->update(['status' => 0]);
        }

        $newGroup = new NewGroup();

        $newGroup->name = request()->input('name');
        $newGroup->category_id = request()->input('category_id');
        $newGroup->status = $this->checkStatus();
        $newGroup->save();
        return $newGroup;

    }

    public function checkStatus() {
        if(request()->input('status') == null) return 0;
        return 1;
    }
}
