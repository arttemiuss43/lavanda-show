<?php

namespace App\Http\Controllers\Admin;

use Arniro\Admin\Http\Controllers\Controller;
use App\Admin\User as UserResource;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        return UserResource::fetch();
    }

    public function show(User $user)
    {
        return UserResource::make($user)->get();
    }

    public function edit(User $user)
    {
        return UserResource::make($user)->get();
    }

    public function update(User $user)
    {
        return tap($user)->update(
            request()->validate([
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
            ])
        );
    }

    public function create()
    {
        return UserResource::make(new User)->get();
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'nullable',
        ]);
        $data['password'] = bcrypt(str_random(8));

        return User::create($data);
    }
}
