<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductVariation;

class CardsController extends Controller
{
    public function index()
    {
        return ProductVariation::with(['filters','images', 'product.images' => function($q){
            $q->limit(1);
        }])->whereIn('id', request('productsId'))->get();
    }

    public function checkAvailability()
    {
        $search = ProductVariation::find(request('id'));
        if((int)request('quantity') > (int)$search->quantity) return ['no', $search->quantity];
        return ['yes', $search->quantity];
    }
}
