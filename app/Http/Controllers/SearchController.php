<?php

namespace App\Http\Controllers;


use App\Category;
use App\ProductVariation;
use Illuminate\Support\Facades\Config;

class SearchController extends Controller
{

    public function handle()
    {
        $variation = ProductVariation::with('product.images', 'images')->where('admin_confirm', '=', true);
        $productVariations = ProductVariation::search(request()->get('query'))
            ->constrain($variation)
            ->orderBy('name')
            ->paginate(16);

        return view('search', [
            'productVariations' => $productVariations,
        ]);
    }

    public function suggestions()
    {
        $categories = Category::where('name->' . Config::get('app.locale'), 'like', '%' . request()->get('query') . '%')->get();
        $variation = ProductVariation::with('product.images', 'images')->with('product.category')->where('admin_confirm', '=', true);
        $productVariations = ProductVariation::search(request()->get('query'))
            ->constrain($variation)
            ->orderBy('name')
            ->get();

        return response()->json([
            'categories' => $categories,
            'variations' => $productVariations
        ]);
    }

    public function catalog($category)
    {
        $category = Category::where('slug', $category)->first();
        $slug = $category->slug;

        $parent = $category->parent;

        while($parent){
            $slug = $parent->slug . '/' . $slug;
            $parent = $parent->parent;
        }

        return redirect('catalog/' . $slug);
    }
}
