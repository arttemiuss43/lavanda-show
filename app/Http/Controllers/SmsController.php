<?php

namespace App\Http\Controllers;

use App\Auth\JWT;
use App\Sms\SendMessage;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\JWTAuth;


class SmsController extends Controller
{
    public function sendRegisterCode()
    {
        $messages = [
            'regex' => 'Введите ваши данные правильно',
            'phone.unique' => 'Пользователь с таким Телефоным номером уже существует.',
        ];
        request()->validate([
            'phone' => ['required', 'unique:users', 'regex:/^\+996\s(?:7|5|9)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/'],
        ], $messages);

        $phone = request()->phone;

        $code = rand(10000, 99999);

        $registerCode = DB::table('register_codes')->where(['phone' => request('phone')])->first();

        if ($registerCode) {

            DB::table('register_codes')->where(['phone' => $phone])->update(['code' => $code]);

        } else {

            DB::table('register_codes')->insert([
                'phone' => $phone,
                'code' => $code,
                'created_at' => Carbon::now()
            ]);

        }


        $phone = str_replace(' ', '', $phone);
        $message = 'Ваш код для регистрации - ' . $code;

        $send = new SendMessage($phone, $message);

        if ($send->handle()) {
            return response()->json(compact('phone'));
        }

        return response()->json(['errors' => [
            'send' => ['Ошибка при отправке смс!']
        ]], 422);

    }


    public function enterByPhone(Request $request)
    {
        $messages = [
            'regex' => 'Введите ваши данные правильно',
        ];
        $request->validate([
            'phone' => ['required', 'regex:/^\+996\s(?:7|5|9)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/'],
        ], $messages);

        $phone = $request->get('phone');
        if (User::where('phone', $phone)->where('type_registered', 'email')->count()) {
            $type_registered = 'email';
            return response()->json(compact('phone', 'type_registered'));
        }

        $code = rand(1000, 9999);
        $registerCode = DB::table('register_codes')->where(['phone' => $phone])->first();
        if ($registerCode) {
            DB::table('register_codes')->where(['phone' => $phone])->update(['code' => $code]);
        } else {
            DB::table('register_codes')->insert([
                'phone' => $phone,
                'code' => $code,
            ]);
        }

        $phoneTrim = str_replace(' ', '', $phone);
        $message = 'Ваш код - ' . $code;

        $send = new SendMessage($phoneTrim, $message);

        if ($send->handle()) {
            $type_registered = 'phone';
            return response()->json(compact('phone', 'type_registered'));
        }

        return response()->json(['errors' => [
            'send' => ['Ошибка при отправке смс!']
        ]], 422);

    }

    public function forUpdatePhone(Request $request)
    {
        $user = \auth()->user();

        $message = 'Пользователь с таким телефонным номером уже существует.';

        if ($user->phone == request('phone')) {
            $message = 'Введите новый телефонный номер.';
        }

        request()->validate([
            'phone' => ['required', Rule::unique('users'), 'regex:/^\+996\s(?:7|5|3)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/']
        ], [
            'phone.regex' => 'Введите ваши данные правильно',
            'phone.unique' => $message,
        ]);


        $phone = $request->get('phone');

        $code = rand(1000, 9999);

        $registerCode = DB::table('register_codes')->where(['phone' => $phone])->first();
        if ($registerCode) {
            DB::table('register_codes')->where(['phone' => $phone])->update(['code' => $code]);
        } else {
            DB::table('register_codes')->insert([
                'phone' => $phone,
                'code' => $code,
            ]);
        }

        $phoneTrim = str_replace(' ', '', $phone);
        $message = 'Ваш код - ' . $code;

        $send = new SendMessage($phoneTrim, $message);

        if ($send->handle()) {
            return response()->json(compact('phone'));
        }

        return response()->json(['errors' => [
            'send' => ['Ошибка при отправке смс!']
        ]], 422);
    }

    public function validateCode()
    {
        $code = request()->code;

        $registerCode = DB::table('register_codes')->where(['phone' => request('phone')])->first();

        if ($registerCode) {

            if ($registerCode->code == $code) {
                $valid = true;
                return response()->json(
                    compact('valid')
                );
            }

        }
    }

    public function loginPhone(Request $request)
    {
        $messages = [
            'username.phone' => 'Поле номер телефона обязательно для заполнения.',
            'username.code' => 'Поле код обязательно для заполнения.',
        ];
        $request->validate([
            'phone' => 'required',
            'code' => 'required'
        ], $messages);

        $phone = $request->get('phone');

        if (preg_match("/^\+996\s(?:7|5|9)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/", $phone)) {
            $user = User::where('phone', $phone)->first();

            $password = rand(100000, 999999);
            if ($user) {
                if ($user->type_registered == 'phone') {
                    User::where('id', $user->id)->update(['password' => bcrypt($password)]);
                }
            } else {
                $user = User::create(['phone' => $phone, 'password' => bcrypt($password), 'type_registered' => 'phone', 'mq' => $request->get('mq')]);
            }

            Auth::loginUsingId($user->id);

            $user = Auth::user();

            return response()->json($user);
        }

        return response()->json(['errors' => [
            'username' => ['Введите ваши данные правильно']
        ]], 422);
    }
}
