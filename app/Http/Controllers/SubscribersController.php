<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
    public function add()
    {
        $messages = [
            'required' => 'Пожалуйста укажите email адрес',
            'unique' => 'Такой email уже есть в списке подписчиков.'
        ];
        $data = request()->validate([
            'email' => 'required|email|unique:subscribers'
        ], $messages);

        Subscriber::create($data);

        return response()->json(['subscribed' => true]);
    }
}
