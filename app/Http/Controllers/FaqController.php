<?php

namespace App\Http\Controllers;


use App\FaqCategory;
use App\Question;

class FaqController extends Controller
{
    public function index()
    {
        $categories = FaqCategory::with(array('questions' => function($query) {
            $query->orderBy('index', 'asc');
        }))->get();

        return view('faq', ['data'=> [
            'categories' => $categories
        ]]);
    }

    public function question($slug){
        $question = Question::where('slug', $slug)->first();
        if (!$question){
            abort(404);
        }
        return view('question', ['data'=> [
            'question' => $question
        ]]);
    }

    public function positiveComplaint($question){

//        Redis::incr('complaints:questions:' . $question . ':likes');

        return response()->json(true);
    }

    public function negativeComplaint($question){

        request()->validate([
            'message' => 'required',
        ]);

        $message = request('message');

//        Redis::rpush('complaints:questions:' . $question . ':comments', $message);

        return response()->json(true);
    }
}
