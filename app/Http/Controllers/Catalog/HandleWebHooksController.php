<?php

namespace App\Http\Controllers\Catalog;

use App\Services\MoyskladService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

class HandleWebHooksController extends Controller
{
    public function handle(Request $request)
    {
        info('---- webhooks -----', $request->all());
        $moyskladService = new MoyskladService();
        $moyskladService->getStarted();
        $moyskladService->getOurDataForSync();
        $moyskladService->handleWebHook($request->all());

        $moyskladService->syncQuantityProductVariations();

        return response()->json('ok')->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }
}
