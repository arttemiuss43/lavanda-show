<?php

namespace App\Http\Controllers\Catalog;

use App\AddressDelivery;
use App\Category;
use App\Filter;
use App\Http\Controllers\Controller;
use App\ProductVariation;
use Illuminate\Http\Request;
use App\Wish;

class VariationsController extends Controller
{
    public function show($slug, $article)
    {
        $variation = ProductVariation::with(
            'discount',
            'images',
            'product.images',
            'product.specifications',
            'product.category.filterCategories.filters',
            'filters.category.productCategories',
            'product.variations.filters.category',
            'product.variations.adminConfirmedReviews.user'
        )->where('article', $article)
            ->where(function ($q) {
                $q->whereHas('images');
                $q->orWhereHas('product.images');
            })
            ->where('admin_confirm', true)
            ->firstOrFail();

        $variation->withProductImages();

        $variation->product->variations->each->withProductImages();

        $filterProductCategoryIds = '';
        $filterCategoryNames = [];
        foreach ($variation->filters as $filterValue) {
            $filterProductCategoryIds = $filterValue->category->productCategories->pluck('id')->toJson();
            $filterCategoryNames[] = [
                'name' => $filterValue->category->name,
            ];
        }

        $path = '';
        $variationCategories = Category::ancestorsAndSelf($variation->product->category_id)->map(function ($category) use (&$path) {
            $category->path = $path . '/' . $category->slug;
            $path = $category->path;

            return $category;
        });
        $productFilters = [];
        foreach ($variation->product->variations as $productVariation) {
            $variation_filters = $this->getVariationFilters($productVariation);
            $productFilters[] = [
                'variation_article' => $productVariation->article,
                'variation_name' => $productVariation->name,
                'variation_filters' => (count($variation_filters))
                    ? $variation_filters :
                    [[
                        'category' => '',
                        'category_id' => 0,
                        'value' => '',
                        'value_id' => 0,
                    ]],
            ];
        }

        $variation->filterProductcategoryIds = $filterProductCategoryIds;
        $variation->filterCategoryNames = $filterCategoryNames;
        $variation->productFilters = $productFilters;
        $variation->categories = $variationCategories;

        $card = session()->get('basket');
        $variation['card'] = isset($card[$variation->id]);

        $reviewData = $this->getReviewData($variation);

        $variationDelivery = AddressDelivery::whereRaw('lower(json_extract(title, "$.ru")) like ?', "%бишкек%")->first();

        return view('variation', compact('variation', 'reviewData', 'variationDelivery'));
    }

    public function variations()
    {
        $variations = ProductVariation::with(
            'product.images',
            'product.category.filterCategories.filters',
            'filters.category.productCategories',
            'product.variations.filters.category',
            'images'
        )->get();

        $data = [];
        foreach ($variations as $variation) {
            $images = [];

            foreach ($variation->images as $image) {
                $images[]['src'] = $image->src;
            }

            if (!count($images)) {
                $productImages = $variation->product->images;
                if ($productImages->count()) {
                    $images[]['src'] = $productImages->first()->src;
                }
            }


            $filterProductCategoryIds = '';
            $filterCategoryNames = [];
            foreach ($variation->filters as $filterValue) {
                $filterProductCategoryIds = $filterValue->category->productCategories->pluck('id')->toJson();
                $filterCategoryNames[] = [
                    'name' => $filterValue->category->name,
                ];
            }

            $categories = Category::ancestorsAndSelf($variation->product->category_id);
            $productFilters = [];
            foreach ($variation->product->variations as $productVariation) {
                $variation_filters = $this->getVariationFilters($productVariation);
                $productFilters[] = [
                    'variation_article' => $productVariation->article,
                    'variation_name' => $productVariation->name,
                    'variation_filters' => (count($variation_filters))
                        ? $variation_filters :
                        [[
                            'category' => '',
                            'category_id' => 0,
                            'value' => '',
                            'value_id' => 0,
                        ]],
                ];
            }

            $data[] = [
                'id' => $variation->id,
                'title' => $variation->name,
                'slug' => $variation->product->slug,
                'article' => ($variation->article) ? $variation->article : "",
                'quantity' => $variation->quantity,
                'salePrice' => $variation->sale_price,
                'discountPrice' => ($variation->discount_price) ? $variation->discount_price : "",
                'recommended' => $variation->recommended,
                'productId' => $variation->product_id,
                'images' => (count($images)) ? $images : [['src' => '']],
                'filterProductCategoryIds' => $filterProductCategoryIds,
                'filterCategoryNames' => $filterCategoryNames,
                'productFilters' => $productFilters,
                'product' => $variation->product,
                'category' => $variation->product->category->toArray(),
                'categoryId' => $variation->product->category_id,
                'categoryIds' => $categories->pluck('id'),
                'categories' => $categories,
            ];
        }

        return response()->json($data);
    }

    private function getVariationFilters($variation)
    {
        $filterValues = [];
        foreach ($variation->filters as $filterValue) {
            $filterValues[] = [
                'category' => $filterValue->category->name,
                'category_id' => $filterValue->category->id,
                'value' => $filterValue->name,
                'value_id' => $filterValue->id,
            ];
        }
        return $filterValues;
    }


    public function variationsByFilter(Request $request)
    {
        if ($request->has('query')) {
            $filters = \GuzzleHttp\json_decode($request->get('query'), true);

            $orderBy = explode('-', $filters['sort']);

            $orderAttr = $orderBy[0];
            $orderType = $orderBy[1];


            $categoryIds = Category::descendantsAndSelf($filters['category'])->pluck('id')->toArray();

            $query = ProductVariation::with('filters', 'product.category', 'images')
                ->whereHas('product.category', function ($q) use ($categoryIds) {
                    $q->whereIn('id', $categoryIds);
                })
                ->orderBy($orderAttr, $orderType);

            if ($filters['filters']) {
                $filterIds = explode(',', $filters['filters']);
                $query->whereHas('filters', function ($q) use ($filterIds) {
                    $q->whereIn('filter_id', $filterIds);
                });
            }

            $variations = $query->paginate(20);

            return response()->json($variations);
        }

        return response()->json(collect());

    }

    /**
     * @param \Illuminate\Database\Eloquent\Model|null $variation
     * @return array
     */
    private function getReviewData(?\Illuminate\Database\Eloquent\Model $variation): array
    {
        $reviewData = [];
        $reviewData['allReviewsCount'] = 0;
        $reviewData['positiveReviewsCount'] = 0;
        $reviewData[1] = 0;
        $reviewData[2] = 0;
        $reviewData[3] = 0;
        $reviewData[4] = 0;
        $reviewData[5] = 0;
        foreach ($variation->product->variations as $item) {
            $reviewData['allReviewsCount'] += $item->adminConfirmedReviews->count();
            foreach ($item->adminConfirmedReviews as $review) {
                if ($review->rating > 3) {
                    $reviewData['positiveReviewsCount']++;
                }
                $reviewData[$review->rating]++;
            }
        }

        $reviewData['positiveReviewsPercent'] = $this->getPercent($reviewData['positiveReviewsCount'], $reviewData['allReviewsCount']);
        $reviewData[1] = $this->getPercent($reviewData[1], $reviewData['allReviewsCount']);
        $reviewData[2] = $this->getPercent($reviewData[2], $reviewData['allReviewsCount']);
        $reviewData[3] = $this->getPercent($reviewData[3], $reviewData['allReviewsCount']);
        $reviewData[4] = $this->getPercent($reviewData[4], $reviewData['allReviewsCount']);
        $reviewData[5] = $this->getPercent($reviewData[5], $reviewData['allReviewsCount']);
        return $reviewData;
    }

    private function getPercent($x, $y)
    {
        if ($x) {
            return round($x * 100 / $y);
        }
        return $x;
    }

}
