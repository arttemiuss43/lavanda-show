<?php

namespace App\Http\Controllers\Catalog;

use App\Category;
use App\Filter;
use App\FilterCategory;
use App\Http\Controllers\Controller;
use App\ProductVariation;

class FiltersController extends Controller
{
    public function filters($productCategoryId)
    {
        $productCategoryIds = Category::descendantsAndSelf($productCategoryId)->pluck('id')->toArray();

        $data = FilterCategory::with('productCategories', 'filters')
            ->whereHas('productCategories', function ($q) use ($productCategoryIds){
                $q->whereIn('category_id', $productCategoryIds);
            })
            ->get();

        return response()->json($data);
    }
}
