<?php

namespace App\Http\Controllers\Catalog;

use App\AddressDelivery;
use App\Http\Controllers\Controller;
use App\ProductVariation;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    public function index()
    {
        $delivery = AddressDelivery::where('title->ru', 'Бишкек')->first();

        if (!$delivery) {
            $delivery = [
                'price' => 200,
                'limit' => 2000,
                'days' => 1
            ];
        }

        return view('catalog.basket', [
            'data' => $this->getBasketData(),
            'delivery' => $delivery
        ]);
    }

    public function addBasket()
    {
        $variationId = \request()->get('id');

        $card = session()->get('basket');
        session()->forget('basket');
        $card[$variationId] = [
            'id' => $variationId,
            'quantity' => request()->get('quantity'),
            'sum' => request()->get('sum'),
            'discount' => request()->get('discount')
        ];
        session()->put('basket', $card);
        $card = session()->get('basket');

        return response()->json(['card' => $card]);
    }


    public function removeBasket()
    {
        $variationId = \request()->get('id');
        $card = session()->get('basket');
        session()->forget('basket');
        unset($card[$variationId]);
        session()->put('basket', $card);

        return response()->json([
            'data' => $this->getBasketData()
        ]);
    }

    public function updateBasket()
    {
        $variationId = \request()->get('id');
        $card = session()->get('basket');
        session()->forget('basket');
        $card[$variationId]['quantity'] = request()->get('quantity');
        $card[$variationId]['sum'] = request()->get('sum');
        $card[$variationId]['discount'] = request()->get('discount');
        session()->put('basket', $card);

        return response()->json([
            'data' => $this->getBasketData()
        ]);
    }

    private function getBasketData()
    {
        $card = session()->get('basket');
        $variations = [];
        if ($card) {
            $variationIds = array_keys($card);
            $variations = ProductVariation::with(
                'images', 'filters',
                'product.images', 'discount'
            )->whereIn('id', $variationIds)->get();

            $wishes = [];

            if(Auth::check()){
                $user = auth()->user();
                $wishes = $user->wishes->pluck('product_variation_id')->toArray();
            }

            foreach ($variations as $variation) {
                $variation['card'] = $card[$variation->id];
                $variation['liked'] = in_array($variation->id, $wishes);
            }
        }

        return [
            'variations' => $variations,
        ];
    }
}
