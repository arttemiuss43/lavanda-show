<?php

namespace App\Http\Controllers\Catalog;

use App\Category;
use App\Filter;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductVariation;

class ProductsController extends Controller
{
    public function products()
    {
        $products = Product::with( 'specifications', 'category')->get();

        $data = [];
        foreach ($products as $product){
            $data[] = [
                'id' => $product->id,
                'title' => $product->name,
                'slug' => $product->slug,
                'des' => $product->des,
                'seoTitle' => $product->seo_title,
                'seoKeywords' => $product->seo_keywords,
                'seoDes' => $product->seo_des,
                'userId' => $product->user_id,
                'categoryId' => $product->category_id,
                'categoryName' => $product->category->name,
                'categoryIds' => Category::ancestorsAndSelf($product->category_id)->pluck('id')->toJson(),
                'filters' => $product->filters,
            ];
        }

        return response()->json($data);
    }

}
