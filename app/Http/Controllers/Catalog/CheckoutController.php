<?php

namespace App\Http\Controllers\Catalog;

use App\Address;
use App\AddressDelivery;
use App\Order;
use App\ProductVariation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        if (!$user) {
            return redirect()->to('/login');
        }

        $card = session()->get('basket');
        $variations = [];
        if ($card) {
            $variationIds = array_keys($card);
            $variations = ProductVariation::with(
                'images', 'filters',
                'product.images', 'discount'
            )->whereIn('id', $variationIds)->get();

            foreach ($variations as $variation) {
                $variation['card'] = $card[$variation->id];
            }
        }

        return view('catalog.checkout', [
            'data' => [
                'addresses' => Address::where('user_id', $user->id)->get(),
                'destinations' => AddressDelivery::get(),
                'variations' => $variations,
            ]
        ]);
    }
}
