<?php

namespace App\Http\Controllers\Catalog;

use App\Category;
use App\FilterCategory;
use App\Filters\VariationFilters;
use App\Http\Controllers\Controller;
use App\ProductVariation;
use App\Services\CategoryService;

class CategoriesController extends Controller
{

    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function category($category, VariationFilters $filters)
    {
        $slugs = explode('/', $category);
        $slug = end($slugs);

        $data = $this->categoryService->getCategoryData($slug);
        return view('category', $data);
    }

}
