<?php

namespace App\Http\Controllers;


use App\Category;
use App\Product;
use App\ProductVariation;
use App\Question;
use App\Services\CategoryService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;


class XmlController extends Controller
{
    public function siteMap()
    {
        $data = null;
        if (Cache::has('site_map_data')) {
            $data = Cache::get('site_map_data');
        } else {
            $data = Cache::remember('site_map_data', 60 * 60 * 24, function () {
                $products = Product::with('variations.images', 'images')
                    ->whereHas('variations', function ($q) {
                        $q->where('admin_confirm', true);
                    })
                    ->where(function ($q) {
                        $q->whereHas('variations.images');
                        $q->orWhereHas('images');
                    })->get();

                $categoryService = new CategoryService();
                return [
                    'categories' => $categoryService->pathCategories(),
                    'products' => $products,
                    'questions' => Question::get()
                ];
            });
        }

        return Response::view('sitemap', $data)->header('Content-Type', 'application/xml');

    }


    public function catalogData()
    {
        $cacheName = 'catalog_xml';
//        if (!Cache::has($cacheName)) {
            $categories = Category::get();
            $variations = ProductVariation::with('filters.category', 'product.category')->get();

            $data = [
                'categories' => $categories,
                'variations' => $variations,
            ];

            Cache::put($cacheName, $data, now()->addDays(1));
//        }

        return response()->view('catalog', Cache::get($cacheName))->header('Content-Type', 'text/xml');
    }
}
