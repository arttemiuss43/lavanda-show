<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Wish;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class TokensController extends Controller {

    public function me()
    {
        $user = auth()->user();
        
        if (!$user) {
            return response()->json(false);
        }

        $user->wishes = Wish::where('user_id', $user->id)
            ->pluck('product_variation_id')->toArray();

        return response()->json($user ?? false);
    }
    
    public function refresh()
    {
        $oldToken = JWTAuth::getToken();
        $token = JWTAuth::refresh($oldToken);

        return response()->json(compact('token'));
    }

    public function sitekey()
    {
        $sitekey = env('GOOGLE_RECAPTCHA_SITEKEY') ?? '6LcYbI0UAAAAAO3asHvWUKeUdFSjI8K8lC5FnkHJ';

        return response()->json($sitekey);
    }
}
