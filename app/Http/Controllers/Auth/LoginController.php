<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Wish;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $username;

    public function __construct()
    {
        $this->username = request()->get('loginType');
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return $this->username;
    }

    protected function authenticated(Request $request, $user)
    {
        return response()->json(array_merge([
                'message' => 'Successfully logged in',
            ], session('url', []))
        );
    }

    protected function loggedOut(Request $request)
    {
        return response()->json(['message' => 'Successfully logged out']);
    }
}
