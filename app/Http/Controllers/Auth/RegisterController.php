<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\SendNotificationOfCreationNewUser;
use App\Mail\ResetPassword;
use App\Sms\SendMessage;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'email.unique' => 'Пользователь с таким E-mail уже существует.',
            'phone.unique' => 'Пользователь с таким номером уже существует.',
            'confirmed' => 'Пароли не совпадают',
            'password.regex' => 'Пароль должен содержать буквы и цифры',
            'phone.regex' => 'Введите ваши данные правильно',
            'code.regex' => 'Введите код правильно',
        ];
        return Validator::make($data, [
            'phone' => ['required','unique:users','regex:/^\+996\s(?:7|5|9)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/'],
            'code' => ['required', 'regex:/[0-9]{5}/'],
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed|regex:/^(?=.*?[0-9])(?=.*?[a-z]).{6,}$/',
            'password_confirmation' => 'required'
        ], $messages);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $registerCode = DB::table('register_codes')->where(['phone' => request('phone')])->first();

        if (!$registerCode || $registerCode->code != request('code')) {
            return response()->json(['errors' => [
                'code' => ['Введите код правильно'],
            ]], 422);
        }

        DB::table('register_codes')->where(['phone' => request('phone')])->delete();

        $user = User::create([
            'phone' => request('phone'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'mq' => request('mq')
        ]);

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        return response()->json($user);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'user' => auth()->user(),
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    public function resetPassword()
    {
        $messages = [
            'username.required' => 'Поле E-mail или номер телефона обязательно для заполнения.',
        ];
        request()->validate([
            'username' => 'required',
        ], $messages);

        if (filter_var(request()->username, FILTER_VALIDATE_EMAIL)) {

            $email = request('username');

            $user = User::where('email', $email)->first();

            if (!$user) {
                return response()->json(['errors' => [
                    'username' => ['Введите ваши данные правильно']
                ]], 422);
            }

            $oldToken = DB::table('password_resets')->where('email', $email)->first();

            if($oldToken) {
                $token = $oldToken->token;
            }
            else {

                $token = null;
                $resetToken = null;

                do {
                    $token = str_random(60);

                    $resetToken = DB::table('password_resets')->where('token', $token)->first();

                }while($resetToken);

                DB::table('password_resets')->insert([
                    'email'=>$email,
                    'token'=>$token,
                    'created_at' => Carbon::now()
                ]);
            }

            Mail::to($email)->send(new ResetPassword($user, $token));

            $type = 'email';
            return response()->json(compact( 'type'));

        }

        if (preg_match("/^\+996\s(?:7|5|9)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/", request()->username)) {

            $phone = request()->username;

            $user = User::where('phone', $phone)->first();

            if (!$user) {
                return response()->json(['errors' => [
                    'email' => ['Введите ваши данные правильно']
                ]], 422);
            }

            $oldCode = DB::table('register_codes')->where('phone', $phone)->first();

            if($oldCode) {
                $code = $oldCode->code;
            }
            else {

                $code = null;
                $resetCode = null;

                do {
                    $code = rand(10000,99999);

                    $resetCode = DB::table('register_codes')->where('code', $code)->first();

                }while($resetCode);

                DB::table('register_codes')->insert([
                    'phone'=>$phone,
                    'code'=>$code,
                    'created_at' => Carbon::now()
                ]);
            }

            $phone = str_replace(' ', '', $phone);
            $message = 'Ваш код для восстановления пароля - ' . $code;

            $send = new SendMessage($phone, $message);

            if($send->handle()){
                $type = 'phone';
                return response()->json(compact('type'));
            }

            return response()->json(['errors' => [
                'send' => ['Ошибка при отправке смс!']
            ]], 422);

        }

        return response()->json(['errors' => [
            'username' => ['Введите ваши данные правильно']
        ]], 422);

    }

    public function changePassword ()
    {
        request()->validate([
            'password' => 'required|min:6|confirmed|regex:/^(?=.*?[0-9])(?=.*?[a-z]).{6,}$/',
            'password_confirmation' => 'required',
            'recaptcha_token' => 'required',
            'token' => 'required',
        ],
            [
                'confirmed' => 'Пароли не совпадают.',
                'recaptcha_token.required' =>  'Пожалуйста, подтвердите, что вы человек!',
                'password.regex' => 'Пароль должен содержать буквы и цифры'
            ]);

        $client = new Client();

        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params'=>
                [
                    'secret' => env('GOOGLE_RECAPTCHA_SECRET') ?? '6LdWWbEUAAAAADIe-JPYJ5eT7kqnhBycp910rona',
                    'response'=> request()->recaptcha_token
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());

        if(!$body->success){
            return response()->json(['errors' => [
                'recaptcha_token' => ['Пожалуйста, подтвердите заново!'],
            ]], 422);
        }

        $resetToken = DB::table('password_resets')->where(['token' => request('token')])->first();

        if ($resetToken) {
            $user = User::where('email', $resetToken->email);
            $user->update(['password' => bcrypt(request('password'))]);
            DB::table('password_resets')->where(['token' => request('token')])->delete();
            return response()->json(['message' => 'Пароль успешно изменен!']);
        }

        return response()->json(['errors' => [
            'token' => ['Токен не действителен'],
        ]], 422);

    }

    public function changePasswordByCode()
    {
        request()->validate(
            [
                'code' => ['required', 'regex:/[0-9]{5}/'],
                'password' => 'required|min:6|confirmed|regex:/^(?=.*?[0-9])(?=.*?[a-z]).{6,}$/',
                'password_confirmation' => 'required',
                'recaptcha_token' => 'required'
            ],
            [
                'confirmed' => 'Пароли не совпадают.',
                'recaptcha_token.required' =>  'Пожалуйста, подтвердите, что вы человек!',
                'code.regex' => 'Введите код правильно',
                'password.regex' => 'Пароль должен содержать буквы и цифры'
            ]
        );

        $client = new Client();

        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params'=>
                [
                    'secret' => env('GOOGLE_RECAPTCHA_SECRET') ?? '6LcYbI0UAAAAAPxXl1roxS-x3Atc0tgVGOmF45ye',
                    'response'=> request()->recaptcha_token
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());

        if(!$body->success){
            return response()->json(['errors' => [
                'recaptcha_token' => ['Пожалуйста, подтвердите заново!'],
            ]], 422);
        }

        $resetCode = DB::table('register_codes')->where(['code' => request('code')])->first();

        if ($resetCode) {
            $user = User::where('phone', $resetCode->phone);
            $user->update(['password' => bcrypt(request('password'))]);
            DB::table('register_codes')->where(['code' => request('code')])->delete();
            return response()->json(['message' => 'Пароль успешно изменен!']);
        }

        return response()->json(['errors' => [
            'code' => ['Код не действителен'],
        ]], 422);

    }
}
