<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset()
    {
        $rules = [
            'password' => 'required|min:6|confirmed|regex:/^(?=.*?[0-9])(?=.*?[a-z]).{6,}$/',
            'password_confirmation' => 'required',
            'recaptcha_token' => 'required',
        ];

        if (\request()->get('code')) {
            $rules['code'] = 'required';
        } else {
            $rules['token'] = 'required';
        }

        request()->validate($rules, [
            'confirmed' => 'Пароли не совпадают.',
            'recaptcha_token.required' => 'Пожалуйста, подтвердите, что вы человек!',
            'password.regex' => 'Пароль должен содержать буквы и цифры'
        ]);

        $client = new Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params' =>
                [
                    'secret' => config('app.recaptcha'),
                    'response' => request()->recaptcha_token
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());

        if (!$body->success) {
            return response()->json(['errors' => [
                'recaptcha_token' => ['Пожалуйста, подтвердите заново!'],
            ]], 422);
        }

        if (\request()->get('code')) {
            $resetCode = DB::table('register_codes')->where(['code' => request('code')])->first();
            if (!$resetCode) {
                return response()->json(['errors' => [
                    'code' => ['Код не действителен'],
                ]], 422);
            }
            $user = User::where('email', $resetCode->phone);
            $user->update(['password' => bcrypt(request('password'))]);
            DB::table('register_codes')->where(['code' => request('code')])->delete();
            return response()->json(['message' => 'Пароль успешно изменен!']);
        } else {
            $resetToken = DB::table('password_resets')->where(['token' => request('token')])->first();

            if (!$resetToken) {
                return response()->json(['errors' => [
                    'token' => ['Токен не действителен'],
                ]], 422);
            }

            $user = User::where('email', $resetToken->email);
            $user->update(['password' => bcrypt(request('password'))]);
            DB::table('password_resets')->where(['token' => request('token')])->delete();
            return response()->json(['message' => 'Пароль успешно изменен!']);

        }
    }
}
