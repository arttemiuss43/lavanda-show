<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Wish;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class JWTLoginController extends Controller
{
    public function authenticate()
    {
        $messages = [
            'username.required' => 'Поле E-mail или номер телефона обязательно для заполнения.',
        ];
        request()->validate([
            'username' => 'required',
            'password' => 'required'
        ], $messages);

        if (filter_var(request()->username, FILTER_VALIDATE_EMAIL)) {
            $email = request()->username;

            $user = User::where('email', $email)->first();

            if (!$user) {
                return response()->json(['errors' => [
                    'password' => ['Email или пароль неверный']
                ]], 422);
            }

        } else if (preg_match("/^\+996\s(?:7|5|9)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/", request()->username)) {


            $phone = request()->username;

            $user = User::where('phone', $phone)->first();

            if (!$user) {
                return response()->json(['errors' => [
                    'password' => ['Телефон или пароль неверный']
                ]], 422);
            }
        } else {
            return response()->json(['errors' => [
                'username' => ['Введите ваши данные правильно']
            ]], 422);
        }

        $credentials = ["id" => $user->id, "password" => request()->password];
        $token = JWTAuth::attempt($credentials);

        if (!$token) {
            return response()->json(['errors' => [
                'password' => ['Email или пароль неверный']
            ]], 422);
        }

        $user = Auth::user();

        $user->wishes = Wish::where('user_id', $user->id)
            ->pluck('product_variation_id')->toArray();

        return response()->json(compact('token', 'user'));
    }


    public function authenticateByPhone(Request $request)
    {
        $messages = [
            'username.phone' => 'Поле номер телефона обязательно для заполнения.',
            'username.code' => 'Поле код обязательно для заполнения.',
        ];
        $request->validate([
            'phone' => 'required',
            'code' => 'required'
        ], $messages);

        $phone = $request->get('phone');

        if (preg_match("/^\+996\s(?:7|5|9)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/", $phone)) {
            $user = User::where('phone', $phone)->first();

            $password = rand(100000, 999999);
            if ($user) {
                if ($user->type_registered == 'phone') {
                    User::where('id', $user->id)->update(['password' => bcrypt($password)]);
                }
            } else {
                $user = User::create(['phone' => $phone, 'password' => bcrypt($password), 'type_registered' => 'phone', 'mq' => $request->get('mq')]);
            }

            // auth()->login($user);

            // return $this->respondWithToken(JWTAuth::fromUser($user));

            Auth::loginUsingId($user->id);
            $token = JWTAuth::attempt(['id' => Auth::id(), 'password' => $password]);

            return response()->json(compact('token', 'user'));
        }

        return response()->json(['errors' => [
            'username' => ['Введите ваши данные правильно']
        ]], 422);
    }

    public function logout()
    {
        return response()->json(['message' => 'Successfully logged out']);
    }
}
