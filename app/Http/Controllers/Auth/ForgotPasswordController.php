<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Sms\SendMessage;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $messages = [
            'username.required' => 'Поле E-mail или номер телефона обязательно для заполнения.',
        ];
        request()->validate([
            'username' => 'required',
        ], $messages);

        if (filter_var(request()->username, FILTER_VALIDATE_EMAIL)) {

            $email = request('username');

            $user = User::where('email', $email)->first();

            if (!$user) {
                return response()->json(['errors' => [
                    'username' => ['Введите ваши данные правильно']
                ]], 422);
            }

            $oldToken = DB::table('password_resets')->where('email', $email)->first();

            if ($oldToken) {
                $token = $oldToken->token;
            } else {
                $token = null;
                $resetToken = null;
                do {
                    $token = str_random(60);
                    $resetToken = DB::table('password_resets')->where('token', $token)->first();
                } while ($resetToken);

                DB::table('password_resets')->insert([
                    'email' => $email,
                    'token' => $token,
                    'created_at' => Carbon::now()
                ]);
            }

            Mail::to($email)->send(new ResetPassword($user, $token));

            $type = 'email';
            return response()->json(compact('type'));

        }

        if (preg_match("/^\+996\s(?:7|5|3)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/", request()->username)) {

            $phone = request()->username;

            $user = User::where('phone', $phone)->first();

            if (!$user) {
                return response()->json(['errors' => [
                    'email' => ['Введите ваши данные правильно']
                ]], 422);
            }

            $code = null;
            $resetCode = null;

            $code = rand(10000, 99999);
            DB::table('register_codes')->where('phone', $phone)->delete();
            DB::table('register_codes')->insert([
                'phone' => $phone,
                'code' => $code,
                'created_at' => Carbon::now()
            ]);

            $phone = str_replace(' ', '', $phone);
            $message = 'Ваш код для восстановления пароля - ' . $code;

            $send = new SendMessage($phone, $message);
            if (!$send->handle()) {
                return response()->json(['errors' => [
                    'send' => ['Ошибка при отправке смс!']
                ]], 422);
            }
            $type = 'phone';
            return response()->json(compact('type'));

        }

        return response()->json(['errors' => [
            'username' => ['Введите ваши данные правильно']
        ]], 422);
    }
}
