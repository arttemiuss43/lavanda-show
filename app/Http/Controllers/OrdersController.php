<?php

namespace App\Http\Controllers;

use App\Order;
use App\Promocode;
use App\Services\Payments\Elsom;
use App\Services\Payments\EpayService;
use App\Services\OrdersService;
use App\Services\RetailcrmService;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;


class OrdersController extends Controller
{
    public function create(OrdersService $ordersService)
    {
        request()->validate([
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'payment' => 'required'
        ]);

        $user = User::find(auth()->id());
        if (!$user->name) {
            $user->name = request()->get('name');
            $user->save();
        }

        $basket = session()->get('basket');

        $order = $ordersService->createOrder($basket);

        if ($order) {
            session()->forget('basket');
            if ($order->payment == 'mastercard') {
                $epayService = new EpayService();
                $epayService->initPayment($order);
                $ePayData = $epayService->generateEpayData();
                if ($ePayData) {
                    return response()->json(compact('ePayData'));
                }
            }

            if ($order->payment == 'elsom'){
                $elsom = new Elsom();
                $otp = $elsom->generateOTP($order->index, $order->amount);
//                if (!$otp){
//                    $order = Order::with('user')->find($order->id);
//                    $this->dispatch(new SendMailToManagerAboutFailPayment($order));
//                }
                return response()->json(compact('order', 'otp'));
            }
            return response()->json(compact('order'));
        }

        return response()->json(['message' => 'The order ended in failure']);
    }

    public function ePayResult($orderIndex)
    {
        $order = Order::where('index', $orderIndex)->first();
        $order->paid = 1;
        $order->save();

        //TODO для теста
        Mail::raw('ePayResult  ' . $orderIndex, function ($message) {
            $message->to('turganbay.030282@gmail.com');
        });
    }

    public function promoUse(Request $request)
    {
        $promoCode = $request->get('promoCode');
        $userId = $request->get('userId');
        $todayDate = Carbon::now();

        $promo = Promocode::where('title', $promoCode)
            ->where('switch', true)
            ->whereDate('start_date', '<=', $todayDate)
            ->whereDate('end_date', '>', $todayDate)
            ->first();

        if (!$promo) {
            return abort(422, 'Такой промокод не существует или истек');
        }

        if ($promo->type === 'Disposable') {
            $orders = Order::where('user_id', $userId)->where('promocode_id', $promo->id)->first();

            if ($orders) {
                return abort(422, 'Вы уже использовали этот промокод');
            }

        }

        return response()->json(compact('promo'));
    }


    public function getOrderByIndex($index)
    {
        $order = Order::with('orderVariations.images', 'orderVariations.filters.category', 'orderVariations.product.images')
            ->where('index', $index)->first();
        return view('account.order', [
            'data' => [
                'order' => $order
            ]
        ]);
    }

    public function returnProduct()
    {
        request()->validate([
            'phone' => 'required',
            'name' => 'required',
            'order_index' => 'required',
            'product_name' => 'required'
        ]);

//        Mail::to(config('mail.mailto.address'), config('mail.mailto.name'))->send(new ReturnProduct(request()->phone,request()->name,request()->order_index,request()->product_name));

        return response()->json(true);
    }
}
