<?php

namespace App\Http\Controllers;

use App\ProductVariation;
use App\User;
use App\Wish;
use Illuminate\Http\Request;

class WishesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wishes = Wish::where('user_id', auth()->user()->id)->pluck('product_variation_id')->all();
        $items = ProductVariation::whereIn('id', $wishes)
            ->with(['images', 'product.images'])->get()
            ->each->withProductImages()->all();

        return view('account.wishes', [
            'data' => [
                'items' => $items
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function store(Request $request)
    {
        auth()->user()->wishes()->create([
            'product_variation_id' => $request->variationId
        ]);

        return [
            'count' => auth()->user()->wishes()->count()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param RequestAlias $request
     * @return void
     */
    public function destroy(Request $request)
    {
        $user = auth()->user();
        Wish::where('product_variation_id', $request->variationId)->where('user_id', $user->id)->first()->delete();
        $data['count'] = Wish::where('user_id','=', $user->id)->count();
        return $data;
    }
}
