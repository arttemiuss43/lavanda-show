<?php

namespace App\Http\Controllers;

use App\AdvertisingBanner;
use App\Banner;
use App\FaqCategory;
use App\MobileBanner;
use App\Question;
use App\Category;
use App\NewGroup;
use App\Product;
use App\RecommendedGroup;
use Illuminate\Support\Facades\Cache;

class PagesController extends Controller
{
    public function home()
    {
        if (!Cache::has('mobile_banners')) {
            $mobileBanners = MobileBanner::orderBy('index', 'asc')->get();
            Cache::put('mobile_banners', MobileBanner::orderBy('index', 'asc')->get(), now()->addMinutes(29));
        }

        if (!Cache::has('banners')) {
            $banners = Banner::orderBy('index', 'asc')->get();
            Cache::put('banners', $banners, now()->addMinutes(25));
        }

        if (!Cache::has('advertising_banner')) {
            $advertisingBanner = AdvertisingBanner::first();
            Cache::put('advertising_banner', $advertisingBanner, now()->addMinutes(22));
        }

        if (!Cache::has('show_home_categories')) {
            $categories = Category::with('images')
                ->whereHas('images')
                ->where('show_home', 1)
                ->get();
            Cache::put('show_home_categories', $categories, now()->addMinutes(15));
        }

        if (!Cache::has('recommended')) {
            $activeGroupRecommended = RecommendedGroup::with([
                'category.products.images',
                'category.products' => function ($q) {
                    $q->whereHas('images');
                    $q->whereHas('variations', function ($query) {
                        $query->where('quantity', '>', 0);
                        $query->where('admin_confirm', true);
                    });
                    $q->take(10);
                },
                'category.descendants.products.images',
                'category.descendants.products' => function ($q) {
                    $q->whereHas('images');
                    $q->whereHas('variations', function ($query) {
                        $query->where('quantity', '>', 0);
                        $query->where('admin_confirm', true);
                    });
                    $q->take(10);
                },
            ])
                ->whereHas('category', function ($q){
                    $q->where('show', 1);
                })
                ->whereHas('category.descendants', function ($q){
                    $q->where('show', 1);
                })
                ->where('status', 1)->get();

            $recommended = [];
            foreach ($activeGroupRecommended as $item) {
                $recommendedItems = collect([]);
                if ($item->category->products->count()) {
                    $recommendedItems->push($item->category->products);
                }
                foreach ($item->category->descendants as $cat) {
                    if ($cat->products->count()) {
                        $recommendedItems->push($cat->products);
                    }
                }
                $recommendedItems = $recommendedItems->collapse();
                if ($recommendedItems->count()) {
                    $recommended[] = [
                        'name' => $item->name,
                        'data' => $recommendedItems,
                        'url' => '/catalog/' . $item->category->slug
                    ];
                }
            }
            Cache::put('recommended', $recommended, now()->addMinutes(39));
        }
        $advertisingBanner = AdvertisingBanner::first();
//        dd($advertisingBanner);

        if (!Cache::has('news')) {
            $activeGroupNews = NewGroup::where('status', 1)->first();

            $news = [];

            if ($activeGroupNews) {
                $newItemsCategories = Category::descendantsAndSelf($activeGroupNews->category_id)
                    ->where('show', 1)
                    ->pluck('id');
                $newItems = Product::with('images')
                    ->whereHas('images')
                    ->whereHas('variations', function ($query) {
                        $query->where('quantity', '>', 0);
                        $query->where('admin_confirm', true);
                    })
                    ->whereIn('category_id', $newItemsCategories)
                    ->take(15)
                    ->get();
                $ancestors = Category::defaultOrder()->ancestorsAndSelf($activeGroupNews->category_id);
                $url = '/catalog';
                foreach ($ancestors as $ancestor) {
                    $url .= '/' . $ancestor->slug;
                }

                $news = [
                    'data' => $newItems,
                    'name' => $activeGroupNews->name,
                    'url' => $url
                ];
            }
            Cache::put('news', $news, now()->addMinutes(38));
        }

        return view('home');
    }

    public function index()
    {
        $faq_categories = FaqCategory::orderBy('index', 'asc')->with('questions')->get();
        $faq_questions = Question::orderBy('index', 'asc')->get();
        $mobile_banners = MobileBanner::orderBy('index', 'asc')->get();
        $banners = Banner::orderBy('index', 'asc')->get();

        return compact('faq_categories', 'faq_questions', 'banners', 'mobile_banners');
    }

    public function menu($lang)
    {
        $categories = Category::all();
        $categories = $categories->toTree();

        return compact('categories');
    }

}
