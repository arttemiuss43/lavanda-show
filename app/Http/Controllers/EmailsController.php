<?php


namespace App\Http\Controllers;


use App\Mail\CallOrder;
use Illuminate\Support\Facades\Mail;

class EmailsController extends Controller
{
    public function sendFeedback () {
        request()->validate([
            'phone' => 'required',
            'name' => 'required'
        ]);

        Mail::to(config('mail.mailto.address'), config('mail.mailto.name'))->send(new CallOrder(request()->phone,request()->name));

        return response()->json(true);
    }

    public function send()
    {
        request()->validate([
            'phone' => 'required',
            'name' => 'required'
        ]);

//        Mail::to(config('mail.mailto.address'), config('mail.mailto.name'))->send(new CallOrder(request()->phone,request()->name));

        return response()->json(true);
    }
}
