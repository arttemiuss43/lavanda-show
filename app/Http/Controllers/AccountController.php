<?php

namespace App\Http\Controllers;

use App\Address;
use App\AddressDelivery;
use App\Distance;
use App\Order;
use App\ProductVariation;
use App\Review;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\JWTAuth;

class AccountController extends Controller
{
    public function index()
    {
        return view('account.index', ['data' => []]);
    }

    public function profile()
    {
        return view('account.profile', [
                'data' => [
                    'addresses' => Address::where('user_id', auth()->id())->get()
                ]
            ]
        );
    }

    public function profileUpdate()
    {
        $user = \auth()->user();
        request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
        ], [
            'email.unique' => 'Пользователь с таким email уже существует.',
        ]);

        $user->update([
            'name' => request('name'),
            'email' => request('email'),
        ]);

        $user = \auth()->user();

        return response()->json(compact('user'));
    }

    public function changePassword()
    {
        if (request()->has('type_registered') && request()->get('type_registered') == 'phone') {
            request()->validate([
                'password' => 'required|min:6|confirmed|regex:/^(?=.*?[0-9])(?=.*?[a-z]).{6,}$/',
                'password_confirmation' => 'required',
            ], [
                'confirmed' => 'Пароли не совпадают.',
                'password.regex' => 'Пароль должен содержать буквы и цифры'
            ]);
            $password = request()->get('password');

            $user = \auth()->user();
            $user->update(['password' => bcrypt($password), 'type_registered' => 'email']);
            $user = \auth()->user();
            return response()->json(compact('user'));
        }

        request()->validate(
            [
                'email' => 'required',
                'current_password' => 'required',
                'password' => 'required|min:6|confirmed|regex:/^(?=.*?[0-9])(?=.*?[a-z]).{6,}$/',
                'password_confirmation' => 'required',
            ],
            [
                'confirmed' => 'Пароли не совпадают.',
                'password.regex' => 'Пароль должен содержать буквы и цифры'
            ]
        );

        $user = \auth()->user();
        if (!Hash::check(request()->current_password, $user->password)) {
            return response()->json(['errors' => [
                'current_password' => ['Пароль неверный']
            ]], 422);
        }
        $user->update(['password' => bcrypt(request('password'))]);
        $user = \auth()->user();
        return response()->json(compact('user'));
    }

    public function changePhone()
    {
        $user = \auth()->user();

        request()->validate([
            'phone' => ['required', Rule::unique('users')->ignore($user->id), 'regex:/^\+996\s(?:7|5|3)[0-9]{2}\s[0-9]{3}\s[0-9]{3}$/']
        ], [
            'phone.regex' => 'Введите ваши данные правильно',
            'phone.unique' => 'Пользователь с таким телефонным номером уже существует.',
        ]);
        $user->update([
            'phone' => request('phone'),
        ]);

        $user = \auth()->user();

        return response()->json(compact('user'));
    }

    public function wishes()
    {
        return view('account.wishes', ['data' => []]);
    }

    public function wishesStore()
    {
        $user = auth()->user();
        if (!$user) {
            return null;
        }
        return WishesController::store;
    }

    public function orders()
    {
        $orders = Order::with('orderVariations.images', 'orderVariations.filters', 'orderVariations.product.images')
            ->where('user_id', \auth()->id())
            ->latest()
            ->get();

        return view('account.orders', [
            'data' => [
                'orders' => $orders
            ]
        ]);
    }
//    public function getAddresses()
//    {
//        $user = auth()->user();
//        if (!$user){
//            return null;
//        }
//
//        $addresses = Address::where('user_id', $user->id)->get();
//        $destinations = AddressDelivery::get();
//        return response()->json(compact('addresses', 'destinations'));
//    }

    public function createAddress()
    {
        request()->validate([
            'user_id' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required'
        ]);

        $address = Address::create([
            'user_id' => request('user_id'),
            'name' => request('name'),
            'surname' => request('surname'),
            'address' => request('address'),
            'city' => request('city'),
            'phone' => request('phone'),
        ]);

        $addresses = Address::where('user_id', request('user_id'))->get();

        return response()->json(
            compact('address', 'addresses')
        );
    }

    public function updateAddress($id)
    {
        request()->validate([
            'user_id' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ]);

        $address = Address::find($id);

        $address->name = request('name');
        $address->surname = request('surname');
        $address->address = request('address');
        if (request('city')) {
            $address->city = request('city');
        }
        $address->phone = request('phone');
        $address->save();

        $addresses = Address::where('user_id', request('user_id'))->get();

        return response()->json(
            compact('address', 'addresses')
        );
    }

    public function deleteAddress($id)
    {
        $address = Address::destroy($id);
        $addresses = Address::where('user_id', auth()->id())->get();

        return response()->json(
            compact('address', 'addresses')
        );
    }

//    public function orderReview($index)
//    {
//        $order = Order::with('orderVariations.images', 'orderVariations.product.images')
//            ->where('user_id', \auth()->id())
//            ->where('index', $index)
//            ->first();
//
//        return view('account.order_review', [
//            'data' => [
//                'order' => $order
//            ]
//        ]);
//    }

    public function orderVariationReview($orderVariationId)
    {
        $orderVariation = DB::table('order_variations')
            ->join('orders', 'order_variations.order_id', '=', 'orders.id')
            ->where('order_variations.id', '=', $orderVariationId)
            ->select('order_variations.*', 'orders.index','orders.created_at')
            ->first();

        return view('account.variation_review', [
            'data' => [
                'productVariation' => ProductVariation::with('images', 'userReview', 'product.images', 'filters.category')
                    ->find($orderVariation->variation_id),
                'orderVariation' => $orderVariation
            ]
        ]);
    }

    public function storeVariationReview()
    {
        request()->validate([
            'comment' => 'required',
        ]);

        \request()->merge([
           'show_name' => filter_var(\request()->get('show_name'), FILTER_VALIDATE_BOOLEAN)
        ]);
        $review = Review::where('user_id', \auth()->id())->where('variation_id', \request()->get('variation_id'))->first();

        if($review){
            $review->update(\request()->all());
            return Review::where('user_id', \auth()->id())->where('variation_id', \request()->get('variation_id'))->first();
        }

        return Review::create(\request()->all());
    }
}
