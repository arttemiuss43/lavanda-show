<?php
function getProductVariationImage($variation){
    if($variation->images->count()){
        $src = $variation->images[0]->src;
    } elseif ($variation->product->images->count()){
        $src = $variation->product->images[0]->src;
    } else {
        $src = 'https://www.brownweinraub.com/wp-content/uploads/2017/09/placeholder.jpg';
    }
    return $src;
}

function getProductVariationImages($variation) {
    if($variation->images->count()){
        $images = $variation->images;
    } elseif ($variation->product->images->count()){
        $images = $variation->product->images;
    }
    else {
        $images = [];
    }
    return $images;
}

