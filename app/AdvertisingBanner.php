<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisingBanner extends Model
{
    protected $table = 'advertising_banners';

    protected $fillable = ['title', 'desktop_image', 'mobile_image', 'link'];
}
