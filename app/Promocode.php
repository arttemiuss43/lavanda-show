<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $fillable = [
        'title', 'type_promo', 'type_discount', 'discount',
        'delivery', 'start_date', 'end_date', 'switch',     
    ];
}
