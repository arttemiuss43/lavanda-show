<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    protected $fillable = [
        'id',
        'rating',
        'comment',
        'file',
        'admin_confirm',
        'show_name',
        'variation_id',
        'user_id'
    ];

    public function setFileAttribute($file)
    {
        if (is_object($file) && $file->isValid()) {
            $imageName = 'review_' . uniqid() . '.' . $file->guessClientExtension();
            $uploadPath = storage_path() . '/app/public/images';
            $file->getClientOriginalExtension();
            $file->move($uploadPath, $imageName);
            $this->attributes['file'] = $imageName;
        }
    }

    public function getFileAttribute($value)
    {
        if ($value) {
            return asset('storage/images/' . $value);
        }
    }

    public function variation()
    {
        return $this->belongsTo(ProductVariation::class, 'variation_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
