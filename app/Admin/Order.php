<?php

namespace App\Admin;


use Arniro\Admin\Fields\BelongsTo;
use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\HasMany;
use Arniro\Admin\Fields\Select;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Trix;
use Arniro\Admin\Http\Resources\Resource;
use Carbon\Carbon;

class Order extends Resource
{
    public static $model = 'App\Order';
    public static $search = ['id'];
    protected $icon = 'cart';

    private $statuses = [
        'new' => 'Новый',
        'process' => 'В процессе',
        'completed' => 'Доставлен',
        'canceled' => 'Отменён',
    ];

    protected function title()
    {
        return 'Заказы';
    }

    public function fields()
    {
        return [
			Text::make('Номер заказа', 'index')->hideFromEdit(),
            BelongsTo::make('Пользователь', 'user_id')->options(\App\User::pluck('name', 'id')->toArray()),
			Text::make('Покупатель', 'full_name')->onlyOnDetail(),
			Text::make('Имя покупателя', 'name')->onlyOnEdit(),
			Text::make('Фамилия покупателя', 'surname')->onlyOnEdit(),
			Text::make('Телефон покупателя', 'phone')->onlyOnEdit(),
			Text::make('Город', 'city')->hideFromIndex(),
			Text::make('Адресс', 'address')->hideFromIndex(),
			Text::make('Промокод', 'promocode')->onlyOnDetail(),
//			Text::make('Executor', 'executor')->onlyOnDetail(),
			Text::make('Способ оплаты', 'payment')->hideFromEdit(),
			Text::make('Сумма', 'amount')->hideFromEdit(),
			Boolean::make('Оплачено', 'paid'),
			Text::make('Дата заказа', 'created_at')->hideFromEdit(),
			Text::make('Доставка', 'reported_delivery_date')->hideFromEdit(),
			Text::make('Скидка', 'discount')->onlyOnDetail(),
			Text::make('Тип скидки', 'discount_type')->onlyOnDetail(),
			Text::make('Цена доставки', 'delivery_sum')->onlyOnDetail(),
            Boolean::make('Упаковка', 'present_receipt')->onlyOnDetail(),
			Text::make('Цена упаковки', 'present_package_price')->onlyOnDetail(),
			Trix::make('Текст упаковки', 'present_text')->onlyOnDetail(),
			Text::make('Устройство', 'mq')->hideFromEdit(),
//			Text::make('Статус', 'status')->hideFromEdit(),
            Select::make('Статус', 'status')->options([
                'new' => 'Новая',
                'process' => 'В процессе',
                'completed' => 'Завершен',
            ])->onlyOnEdit(),

            HasMany::make('orderVariations', OrderVariation::class)
        ];
    }

    public function toArray($item)
    {
        return [
            'full_name' => $item->name . ' ' . $item->surname . ' ' . $item->phone,
            'amount' => $item->amount . ' сом',
            'payment' => $item->payment == 'cash' ? 'В наличку' : 'Онлайн',
            'created_at' => optional($item->created_at)->format('d-m-Y')
//            'address' => $item->city . ' ' . $item->address,
//            'status' => isset($this->statuses[$item->status]) ? $this->statuses[$item->status] : $item->status,
        ];
    }
}
