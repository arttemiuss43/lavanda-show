<?php

namespace App\Admin;


use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;

class ProductVariation extends Resource
{
    public static $model = 'App\ProductVariation';
    public static $search = ['article'];

    public function fields()
    {

        return [
            Text::make('Артикул', 'article')->onlyOnIndex(),
            Text::make('Название', 'name'),
            Text::make('Цена', 'sale_price'),
            // Text::make('Скидка', 'discount_price'),
            Text::make('Кол-во', 'quantity'),
            Boolean::make('Подтвержден', 'admin_confirm'),
            Boolean::make('Рекомендован', 'recommended')->onlyOnDetail(),
            Boolean::make('Показать', 'show')->onlyOnDetail()
        ];
    }

    protected function title()
    {
        return 'Вариации';
    }
}
