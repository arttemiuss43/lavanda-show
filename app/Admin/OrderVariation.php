<?php

namespace App\Admin;


use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;

class OrderVariation extends Resource
{
    public static $model = 'App\ProductVariation';
    public static $search = ['article'];

    public function fields()
    {

        return [
            Text::make('Артикул', 'article'),
            Text::make('Название', 'name'),
            Text::make('Кол-во', 'quantity'),
            Text::make('На сумму', 'sum'),
        ];
    }

    protected function title()
    {
        return 'Вариации';
    }

    public function toArray($item)
    {
        return [
            'quantity' => $item->pivot->quantity,
            'sum' => $item->pivot->sum
        ];
    }

    public function create()
    {
        return false;
    }
    public function show()
    {
        return false;
    }

    public function update()
    {
        return false;
    }
}
