<?php


namespace App\Admin;


use Arniro\Admin\Fields\CropImage;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Textarea;
use Arniro\Admin\Http\Resources\Resource;

class Banner extends Resource
{
    public static $model = 'App\\Banner';

    public function fields()
    {
        return [
            Text::make('Заголовок', 'title'),
            Textarea::make('Описание', 'des'),
            CropImage::make('Изображение', 'image')
                ->ratio(7/2),
            Text::make('Ссылка', 'link'),
        ];
    }
}
