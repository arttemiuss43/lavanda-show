<?php


namespace App\Admin;


use Arniro\Admin\Fields\CropImage;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;

class MobileBanner extends Resource
{
    public static $model = 'App\\MobileBanner';

    public function fields()
    {
        return [
            Text::make('Заголовок', 'title'),
            Text::make('Ссылка', 'link'),
            CropImage::make('Изображение', 'image')
            ->ratio(5/3),
        ];
    }
}
