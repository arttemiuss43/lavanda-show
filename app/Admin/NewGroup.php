<?php

namespace App\Admin;


use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Select;
use Arniro\Admin\Http\Resources\Resource;

class NewGroup extends Resource
{
    public static $model = 'App\NewGroup';
    public static $search = ['name'];

    protected $categories;

    protected function setUp()
    {
        $this->categories = \App\Category::get();
    }

    public function fields()
    {
        return [
            Text::make('Название', 'name'),
            Select::make('Категория', 'category_id')->options($this->categories->pluck('name', 'id')->toArray()),
            // Text::make('Категория', 'category_id')->options(\App\Category::get()->toTree()->toArray()),
            Boolean::make('Статус', 'status'),
        ];
    }

    public function title() 
    {
        return 'Новинки';
    }

    public function toArray($item)
    {
        return [
            'categories' => $this->categories->toTree()->map(function ($category) {
                $category->active = false;
    
                return $category;
            })->toArray()
        ];
    }

}