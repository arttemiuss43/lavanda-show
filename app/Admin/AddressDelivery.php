<?php


namespace App\Admin;


use Arniro\Admin\Fields\Select;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;
use App\Admin\Category as CategoryResource;
use App\Admin\Product as ProductResource;
use App\Admin\FilterCategory as FilterCategoryResource;

class AddressDelivery extends Resource
{
    public static $model = 'App\\AddressDelivery';

    public function fields()
    {
        return [
            Text::make('Адрес', 'title'),

            Text::make('Стоимость доставки', 'price'),

            Text::make('Лимит стоимости', 'limit'),

            Select::make('Количество дней', 'days')->options([1, 2, 3]),
        ];
    }
}
