<?php


namespace App\Admin;


use Arniro\Admin\Fields\BelongsTo;
use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Select;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Textarea;
use Arniro\Admin\Http\Resources\Resource;
use App\Admin\Variation as VariationResource;
use App\Admin\Image as ImagesResource;
use App\Admin\Specification as SpecificationsResource;

class Product extends Resource
{
    public static $model = 'App\\Product';

    public static $creatable = false;
    public static $deletable = false;

    public function setUp()
    {
        $user = request()->user();

        if ($user->type === 'seller') {
            self::$creatable = true;
            self::$deletable = true;
        }
    }

    public function fields()
    {
        return [
            BelongsTo::make('Категория', 'category_id')->options(\App\Category::pluck('name', 'id')->toArray() ),
            Text::make('Название', 'name'),
            Textarea::make('Описание', 'des')->hideFromIndex(),
//            Boolean::make('Показать', 'show'),
            Text::make('SEO-заголовок', 'seo_title')->hideFromIndex(),
            Textarea::make('SEO-описание', 'seo_des')->hideFromIndex(),
            Textarea::make('SEO-ключевые слова', 'seo_keywords')->hideFromIndex(),
        ];
    }

    public function relationships($item)
    {
        return [
            'images' => ImagesResource::collection($item->images),
            'variations' => VariationResource::collection($item->variations),
            'specifications' => SpecificationsResource::collection($item->specifications),
        ];
    }
}
