<?php

namespace App\Admin;


use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Image;
use Arniro\Admin\Http\Resources\Resource;

class BottomBanner extends Resource
{
    public static $model = 'App\AdvertisingBanner';
    public static $search = ['id'];

    public function fields()
    {
        return [
            Text::make('Заголовок', 'title'),
            Text::make('Ссылка', 'link'),
            Image::make('Mobile image', 'mobile_image'),
            Image::make('Desktop image', 'desktop_image'),
        ];
    }

    protected function title()
    {
        return 'Нижний баннер';
    }
}
