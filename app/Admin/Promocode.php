<?php


namespace App\Admin;


use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Select;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Date;
use Arniro\Admin\Http\Resources\Resource;


class Promocode extends Resource
{
    public static $model = 'App\\Promocode';

    public function fields()
    {
        return [

            Text::make('Название', 'title'),

            Select::make('Тип промокода', 'type_promo')->options([
                'one' => 'Одноразовый',
                'many' => 'Многоразовый',
            ]),

            Select::make('Тип скидки', 'type_discount')->options([
                'Fixed' => 'Фиксированное значение',
                'Percentage' => 'Процентное значение',
            ]),

            Text::make('Скидка', 'discount'),

            Select::make('Доставка', 'delivery')->options([
                'free' => 'Бесплатное',
                'paid' => 'Платное',
            ]),

            Date::make('Дата начала скидки', 'start_date'),

            Date::make('Дата конца скидки', 'end_date'),

            Boolean::make('Включить', 'switch'),

        ];
    }
}
