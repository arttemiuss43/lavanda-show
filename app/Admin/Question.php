<?php


namespace App\Admin;
use Arniro\Admin\Fields\BelongsTo;
use Arniro\Admin\Fields\Select;
use Arniro\Admin\Fields\Text;
//use Arniro\Admin\Fields\Textarea;
use Arniro\Admin\Fields\Trix;
use Arniro\Admin\Http\Resources\Resource;
use App\FaqCategory;
use http\Client\Request;


class Question extends Resource
{
    public static $model = 'App\\Question';

//    public static $search = ['name'];
    public function fields()
    {
        return [
            BelongsTo::make('Категория', 'faq_category_id')
                ->options(FaqCategory::pluck('name->ru as name','id')->toArray()),
            Text::make('Вопрос', 'name'),
            Trix::make('Ответ', 'answer')
                ->hideFromIndex(),
        ];
    }
}
