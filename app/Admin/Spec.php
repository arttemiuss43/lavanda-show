<?php

namespace App\Admin;


use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;

class Spec extends Resource
{
    public static $model = 'App\Spec';
    public static $search = ['id'];

    public function fields()
    {
        return [
            Text::make('Id', 'id'),
        ];
    }
}
