<?php


namespace App\Admin;


use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Select;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Textarea;
use Arniro\Admin\Http\Resources\Resource;
use App\Admin\Product as ProductResource;
use App\Admin\Image as ImagesResource;


class Category extends Resource
{
    public static $model = 'App\\Category';

    public function fields()
    {
        return [
            Select::make('Категория', 'parent_id')->options(\App\Category::pluck('name', 'id')->toArray())->hideFromIndex(),
            Text::make('Имя', 'name'),
            Textarea::make('Описание', 'des')->hideFromIndex(),
            Boolean::make('Показать', 'show'),
            Boolean::make('На главной странице', 'show_home'),
            Text::make('SEO-заголовок', 'seo_title')->hideFromIndex(),
            Textarea::make('SEO-описание', 'seo_des')->hideFromIndex(),
            Textarea::make('SEO-ключевые слова', 'seo_keywords')->hideFromIndex(),
        ];
    }

    public function relationships($item)
    {
        return [
            'images' => ImagesResource::collection($item->images),
            'products' => ProductResource::collection($item->products),
        ];
    }


    public function toArray($item)
    {
        return [
            'filterCategories' => $item->filterCategories
        ];
    }
}
