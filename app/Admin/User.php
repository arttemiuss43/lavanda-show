<?php


namespace App\Admin;


use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;

class User extends Resource
{
    public static $model = 'App\\User';
    public static $search = ['name'];

    protected $icon = 'user';

    public function fields()
    {
        return [
            Text::make('Имя', 'name'),
            Text::make('Email', 'email'),
            Text::make('Телефон', 'phone'),
            Text::make('Создан', 'created_at')
        ];
    }

    public function title()
    {
        return 'Пользователи';
    }
}
