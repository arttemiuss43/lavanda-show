<?php

namespace App\Admin;


use Arniro\Admin\Fields\Date;
use Arniro\Admin\Fields\Number;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Select;
use App\Admin\ProductVariation;
use Arniro\Admin\Http\Resources\Resource;

class Discount extends Resource
{
    public static $model = 'App\Discount';
    public static $search = ['name'];

    protected $categories;

    protected function setUp()
    {
        $this->categories = \App\Category::get();
    }

    public function fields()
    {
        return [
            Text::make('Название', 'name'),
            Select::make('Категория', 'category_id')
                ->options($this->categories->pluck('name', 'id')->toArray())
                ->hideFromIndex()
                ->hideFromDetail(),
            Number::make('Скидка (%)', 'count')->min(0)->max(100),
            Date::make('Дата начала скидки', 'start'),
            Date::make('Дата окончания скидки', 'end'),
        ];
    }

    public function title() 
    {
        return 'Скидки';
    }

    public function toArray($item)
    {
        return [
            'categories' => $this->categories->toTree()->map(function ($category) {
                $category->active = false;
    
                return $category;
            })->toArray()
        ];
    }

    public function relationships($item)
    {
        return [
            'variations' => ProductVariation::collection($item->variations),
        ];
    }

}
