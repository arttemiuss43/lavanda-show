<?php


namespace App\Admin;

use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;
use App\Admin\Question as QuestionResource;


class FaqCategory extends Resource
{
    public static $model = 'App\\FaqCategory';

    public function fields()
    {
        return [
            Text::make('Категория', 'name'),
//            Text::make('Количество вопросов', 'index')->hideFromCreate(),
        ];
    }

    public function relationships($item)
    {
        return [
            'questions' => QuestionResource::collection($item->questions),
        ];
    }

}
