<?php

namespace App\Admin;


use Arniro\Admin\Fields\BelongsTo;
use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Image;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Fields\Textarea;
use Arniro\Admin\Http\Resources\Resource;

class Review extends Resource
{
    public static $model = 'App\Review';
    public static $search = ['id'];

    public function fields()
    {
        return [
            BelongsTo::make('Продукт', 'variation_id')->options(\App\ProductVariation::pluck('name', 'id')->toArray()),
            BelongsTo::make('Пользователь', 'user_id')->options(\App\User::pluck('name', 'id')->toArray()),
            Image::make('Изображение', 'file'),
            Text::make('Рейтинг', 'rating'),
            Boolean::make('Показать имя', 'show_name'),
            Boolean::make('Подтвежден', 'admin_confirm'),
            Textarea::make('Коментарий', 'comment')->hideOnIndex(),
        ];
    }

    public function title()
    {
        return 'Отзывы';
    }

}
