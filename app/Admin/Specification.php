<?php


namespace App\Admin;

use Arniro\Admin\Fields\BelongsTo;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;

class Specification extends Resource
{
    public static $model = 'App\\Specification';

    public function fields()
    {
        return [
            BelongsTo::make('Продукт', 'product_id')->options(\App\Product::pluck('name', 'id')->toArray()),
            Text::make('Название', 'name'),
            Text::make('Значение', 'value'),
            Text::make('Позиция', 'position'),
        ];
    }

}
