<?php


namespace App\Admin;

use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Http\Resources\Resource;

class Image extends Resource
{
    public static $model = 'App\\Image';

    public function fields()
    {
        return [
            \Arniro\Admin\Fields\Image::make('Изображение', 'src'),
            Boolean::make('Паказать', 'show'),
            Boolean::make('Главное', 'main_image'),
        ];
    }
}
