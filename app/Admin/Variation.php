<?php


namespace App\Admin;


use Arniro\Admin\Fields\Boolean;
use Arniro\Admin\Fields\Select;
use Arniro\Admin\Fields\Text;
use Arniro\Admin\Http\Resources\Resource;
use App\Admin\Image as ImagesResource;
use App\Admin\Filter as FiltersResource;

class Variation extends Resource
{
    public static $model = 'App\\ProductVariation';

    public function fields()
    {
        return [
            Select::make('Продукт', 'product_id')->options(\App\Product::pluck('name', 'id')->toArray()),
            Text::make('Название', 'name'),
            Text::make('Количество', 'quantity'),
            Text::make('Цена', 'sale_price'),
            Text::make('Цена со скидкой', 'discount_price'),
//            Boolean::make('Показать', 'show'),
//            Boolean::make('Рекомендовать', 'recommended'),
            Boolean::make('Подтвердить', 'admin_confirm'),
        ];
    }

    public function relationships($item)
    {
        if (count($item->toArray())) {
            return [
                'productImages' => ImagesResource::collection($item->product->images),
                'images' => ImagesResource::collection($item->images),
            ];
        }
        return [];
    }

    public function toArray($item)
    {
//        if (count($item->toArray())) {
            return [
                'variationFilters' => $item->filters
            ];
//        }
    }
}
