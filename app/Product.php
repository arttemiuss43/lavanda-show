<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use App\Translations\HasTranslations;

class Product extends Model
{
    use HasTranslations;
    use Sluggable;

    protected $table = 'products';

    protected $fillable = [
        'moysklad_id',
        'name',
        'article',
        'des',
        'slug',
        'show',
        'seo_title',
        'seo_keywords',
        'seo_des',
        'user_id',
        'category_id',
        'external_code'
    ];

    public $translatable = ['name', 'des', 'specs','seo_title', 'seo_keywords', 'seo_des'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function setShowAttribute($value)
    {
        $this->attributes['show'] = !!$value;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function variations()
    {
        return $this->hasMany(ProductVariation::class, 'product_id');
    }

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable');
    }

    public function specifications()
    {
        return $this->hasMany(Specification::class, 'product_id');
    }

}
