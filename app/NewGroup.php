<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewGroup extends Model
{
    protected $fillable = [
        'name',
        'category_id',
        'status',
    ];
    public function category(){
        return $this->belongsTo(Category::class, 'category_id');
    }
}
