<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use App\Translations\HasTranslations;

class Category extends Model
{
    use HasTranslations;
    use NodeTrait, Sluggable {
        Sluggable::replicate as replicateSlug;
        NodeTrait::replicate insteadof Sluggable;
    }

    protected $table = 'categories';

    protected $fillable = ['moysklad_id', 'name', 'des', 'parent_id', 'slug', 'show', 'show_home', 'seo_title', 'seo_des', 'seo_keywords'];

    public $translatable = ['name', 'des', 'seo_title', 'seo_des', 'seo_keywords'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function setShowAttribute($value)
    {
        $this->attributes['show'] = !!$value;
    }
    public function setShowHomeAttribute($value)
    {
        $this->attributes['show_home'] = !!$value;
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function filterCategories() {
        return $this->belongsToMany(
            FilterCategory::class,
            'filters_categories',
            'category_id',
            'filter_id'
        );
    }

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable');
    }

    public function getParentIdAttribute()
    {
        return $this->attributes['parent_id'] ?? 0;
    }
}
