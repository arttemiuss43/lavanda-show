<?php

namespace App;

use App\FaqCategory;
use Illuminate\Database\Eloquent\Model;
use App\Translations\HasTranslations;
//use Cviebrock\EloquentSluggable\Sluggable;
use App\Admin\Sluggable;

class Question extends Model
{
    use Sluggable, HasTranslations;

    public $translatable = ['name', 'answer'];
    protected $table = 'questions';

    protected $fillable = [
        'faq_category_id',
        'name',
        'slug',
        'answer',
        'index',
    ];

//    public function sluggable()
//    {
//        return [
//            'slug' => [
//                'source' => 'name'
//            ]
//        ];
//    }

    public function faqCategory()
    {
        return $this->belongsTo(FaqCategory::class);
    }

}
