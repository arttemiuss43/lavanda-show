<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = [
        'name',
        'category_id',
        'status',
        'start',
        'end'
    ];

    protected $dates = ['start', 'end'];

    public function variations()
    {
        return $this->hasMany(ProductVariation::class);
    }
}
